Clients specifications

1 Use three.js to render provided character ()
2 Use pixi.js to render 2D background and the rest of game assets as 2D sprites
3 use Typescript

Gameplay overview
An isometric dungeon crawler with randomly generated levels, destructable tiles and resource managment. Goal of the game is to obtain a rare crystal and exit level as soon as possible.

Gameplay
Character can deploy a bomb that will explode in few seconds destroying nearby tiles. Player have limited amount of bombs. Bombs are crafted automatically when resources are collected. There are two resources to collect sulfur and nitrate.

Controlls and camera
Main character is cotroled by clicking mouse/screen to show where character should move. Camera controlled by mouse drag/finger drag. Camera may only move in certain area around character so that character is always visible on screen.

Milestones
- Setup dev environment and production pipeline 
		deadline 25.01 - status DONE

- Background dirt
- Merry threejs and pixijs
- Camera controlls
- Render character
- Setup three.js lighting
		deadline 26.01 - status DONE

- 2D tiled world with randomly generated levels
- Make character move when clicked on tile (pathfinding)
		deadline 28.01 - status DONE
		
- Tile removal mechanics
- Explosions with decals, particles and sound
- Bomb drop ability
		deadline 29.01 - status IN QUEUE
	
- Pick-ups (sulfur, nitrite, crystal)
- Game restart mechanic
- Simple inventory, auto-crafting
- Tiles soil, rock, floor, nitite, sulfur, exit
- UI
	Inventory: bombs and resources
	Menu: Help, restart
	Loading screen
	Win/lose screen
	Enter name screen
	Timer meter, depth meter
- Backend score board
		deadline 30.01 - status IN QUEUE

- Free 8-bit style tune as background music
- Deployment and bugfixing
		deadline 31.01 - status IN QUEUE
	


