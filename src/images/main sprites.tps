<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>5</int>
        <key>texturePackerVersion</key>
        <string>6.0.0</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <true/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>pixijs4</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrQualityLevel</key>
        <uint>3</uint>
        <key>astcQualityLevel</key>
        <uint>2</uint>
        <key>basisUniversalQualityLevel</key>
        <uint>2</uint>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1QualityLevel</key>
        <uint>70</uint>
        <key>etc2QualityLevel</key>
        <uint>70</uint>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>1</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>0</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>1024</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">Basic</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../dist/resources/tiles.json</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <false/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>0</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">None</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0,0</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">tiles/button_off.png</key>
            <key type="filename">tiles/button_on.png</key>
            <key type="filename">tiles/dynomite_off.png</key>
            <key type="filename">tiles/dynomite_on.png</key>
            <key type="filename">tiles/explosion/explosion_001.png</key>
            <key type="filename">tiles/explosion/explosion_002.png</key>
            <key type="filename">tiles/explosion/explosion_003.png</key>
            <key type="filename">tiles/explosion/explosion_004.png</key>
            <key type="filename">tiles/explosion/explosion_005.png</key>
            <key type="filename">tiles/explosion/explosion_006.png</key>
            <key type="filename">tiles/explosion/explosion_007.png</key>
            <key type="filename">tiles/explosion/explosion_008.png</key>
            <key type="filename">tiles/explosion/explosion_009.png</key>
            <key type="filename">tiles/explosion/explosion_010.png</key>
            <key type="filename">tiles/explosion/explosion_011.png</key>
            <key type="filename">tiles/explosion/explosion_012.png</key>
            <key type="filename">tiles/explosion/explosion_013.png</key>
            <key type="filename">tiles/explosion/explosion_014.png</key>
            <key type="filename">tiles/explosion/explosion_015.png</key>
            <key type="filename">tiles/explosion/explosion_016.png</key>
            <key type="filename">tiles/explosion/explosion_017.png</key>
            <key type="filename">tiles/explosion/explosion_018.png</key>
            <key type="filename">tiles/explosion/explosion_019.png</key>
            <key type="filename">tiles/explosion/explosion_020.png</key>
            <key type="filename">tiles/explosion/explosion_021.png</key>
            <key type="filename">tiles/explosion/explosion_022.png</key>
            <key type="filename">tiles/explosion/explosion_023.png</key>
            <key type="filename">tiles/explosion/explosion_024.png</key>
            <key type="filename">tiles/explosion/explosion_025.png</key>
            <key type="filename">tiles/explosion/explosion_026.png</key>
            <key type="filename">tiles/explosion/explosion_027.png</key>
            <key type="filename">tiles/explosion/explosion_028.png</key>
            <key type="filename">tiles/explosion/explosion_029.png</key>
            <key type="filename">tiles/explosion/explosion_030.png</key>
            <key type="filename">tiles/explosion/explosion_031.png</key>
            <key type="filename">tiles/explosion/explosion_032.png</key>
            <key type="filename">tiles/explosion/explosion_033.png</key>
            <key type="filename">tiles/explosion/explosion_034.png</key>
            <key type="filename">tiles/explosion/explosion_035.png</key>
            <key type="filename">tiles/explosion/explosion_036.png</key>
            <key type="filename">tiles/explosion/explosion_037.png</key>
            <key type="filename">tiles/explosion/explosion_038.png</key>
            <key type="filename">tiles/explosion/explosion_039.png</key>
            <key type="filename">tiles/explosion/explosion_040.png</key>
            <key type="filename">tiles/explosion/explosion_041.png</key>
            <key type="filename">tiles/explosion/explosion_042.png</key>
            <key type="filename">tiles/explosion/explosion_043.png</key>
            <key type="filename">tiles/explosion/explosion_044.png</key>
            <key type="filename">tiles/explosion/explosion_045.png</key>
            <key type="filename">tiles/explosion/explosion_046.png</key>
            <key type="filename">tiles/explosion/explosion_047.png</key>
            <key type="filename">tiles/explosion/explosion_048.png</key>
            <key type="filename">tiles/explosion/explosion_049.png</key>
            <key type="filename">tiles/explosion/explosion_050.png</key>
            <key type="filename">tiles/explosion/explosion_051.png</key>
            <key type="filename">tiles/explosion/explosion_052.png</key>
            <key type="filename">tiles/explosion/explosion_053.png</key>
            <key type="filename">tiles/explosion/explosion_054.png</key>
            <key type="filename">tiles/explosion/explosion_055.png</key>
            <key type="filename">tiles/explosion/explosion_056.png</key>
            <key type="filename">tiles/explosion/explosion_057.png</key>
            <key type="filename">tiles/explosion/explosion_058.png</key>
            <key type="filename">tiles/explosion/explosion_059.png</key>
            <key type="filename">tiles/explosion/explosion_060.png</key>
            <key type="filename">tiles/explosion/explosion_061.png</key>
            <key type="filename">tiles/explosion/explosion_062.png</key>
            <key type="filename">tiles/explosion/explosion_063.png</key>
            <key type="filename">tiles/explosion/explosion_064.png</key>
            <key type="filename">tiles/gems/common/gem_common_000.png</key>
            <key type="filename">tiles/gems/common/gem_common_001.png</key>
            <key type="filename">tiles/gems/common/gem_common_002.png</key>
            <key type="filename">tiles/gems/common/gem_common_003.png</key>
            <key type="filename">tiles/gems/common/gem_common_004.png</key>
            <key type="filename">tiles/gems/common/gem_common_005.png</key>
            <key type="filename">tiles/gems/common/gem_common_006.png</key>
            <key type="filename">tiles/gems/common/gem_common_007.png</key>
            <key type="filename">tiles/gems/common/gem_common_008.png</key>
            <key type="filename">tiles/gems/common/gem_common_009.png</key>
            <key type="filename">tiles/gems/common/gem_common_010.png</key>
            <key type="filename">tiles/gems/common/gem_common_011.png</key>
            <key type="filename">tiles/gems/common/gem_common_012.png</key>
            <key type="filename">tiles/gems/common/gem_common_013.png</key>
            <key type="filename">tiles/gems/common/gem_common_014.png</key>
            <key type="filename">tiles/gems/common/gem_common_015.png</key>
            <key type="filename">tiles/gems/common/gem_common_016.png</key>
            <key type="filename">tiles/gems/common/gem_common_017.png</key>
            <key type="filename">tiles/gems/common/gem_common_018.png</key>
            <key type="filename">tiles/gems/common/gem_common_019.png</key>
            <key type="filename">tiles/gems/common/gem_common_020.png</key>
            <key type="filename">tiles/gems/common/gem_common_021.png</key>
            <key type="filename">tiles/gems/common/gem_common_022.png</key>
            <key type="filename">tiles/gems/common/gem_common_023.png</key>
            <key type="filename">tiles/gems/common/gem_common_024.png</key>
            <key type="filename">tiles/gems/common/gem_common_025.png</key>
            <key type="filename">tiles/gems/common/gem_common_026.png</key>
            <key type="filename">tiles/gems/common/gem_common_027.png</key>
            <key type="filename">tiles/gems/common/gem_common_028.png</key>
            <key type="filename">tiles/gems/common/gem_common_029.png</key>
            <key type="filename">tiles/gems/common/gem_common_030.png</key>
            <key type="filename">tiles/gems/common/gem_common_031.png</key>
            <key type="filename">tiles/gems/common/gem_common_032.png</key>
            <key type="filename">tiles/gems/common/gem_common_033.png</key>
            <key type="filename">tiles/gems/common/gem_common_034.png</key>
            <key type="filename">tiles/gems/common/gem_common_035.png</key>
            <key type="filename">tiles/gems/common/gem_common_036.png</key>
            <key type="filename">tiles/gems/common/gem_common_037.png</key>
            <key type="filename">tiles/gems/common/gem_common_038.png</key>
            <key type="filename">tiles/gems/common/gem_common_039.png</key>
            <key type="filename">tiles/gems/common/gem_common_040.png</key>
            <key type="filename">tiles/gems/common/gem_common_041.png</key>
            <key type="filename">tiles/gems/common/gem_common_042.png</key>
            <key type="filename">tiles/gems/common/gem_common_043.png</key>
            <key type="filename">tiles/gems/common/gem_common_044.png</key>
            <key type="filename">tiles/gems/common/gem_common_045.png</key>
            <key type="filename">tiles/gems/common/gem_common_046.png</key>
            <key type="filename">tiles/gems/common/gem_common_047.png</key>
            <key type="filename">tiles/gems/common/gem_common_048.png</key>
            <key type="filename">tiles/gems/common/gem_common_049.png</key>
            <key type="filename">tiles/gems/common/gem_common_050.png</key>
            <key type="filename">tiles/gems/common/gem_common_051.png</key>
            <key type="filename">tiles/gems/common/gem_common_052.png</key>
            <key type="filename">tiles/gems/common/gem_common_053.png</key>
            <key type="filename">tiles/gems/common/gem_common_054.png</key>
            <key type="filename">tiles/gems/common/gem_common_055.png</key>
            <key type="filename">tiles/gems/common/gem_common_056.png</key>
            <key type="filename">tiles/gems/common/gem_common_057.png</key>
            <key type="filename">tiles/gems/common/gem_common_058.png</key>
            <key type="filename">tiles/gems/common/gem_common_059.png</key>
            <key type="filename">tiles/gems/common/gem_common_060.png</key>
            <key type="filename">tiles/gems/common/gem_common_061.png</key>
            <key type="filename">tiles/gems/common/gem_common_062.png</key>
            <key type="filename">tiles/gems/common/gem_common_063.png</key>
            <key type="filename">tiles/gems/common/gem_common_064.png</key>
            <key type="filename">tiles/gems/common/gem_common_065.png</key>
            <key type="filename">tiles/gems/common/gem_common_066.png</key>
            <key type="filename">tiles/gems/common/gem_common_067.png</key>
            <key type="filename">tiles/gems/common/gem_common_068.png</key>
            <key type="filename">tiles/gems/common/gem_common_069.png</key>
            <key type="filename">tiles/gems/common/gem_common_070.png</key>
            <key type="filename">tiles/gems/common/gem_common_071.png</key>
            <key type="filename">tiles/gems/common/gem_common_072.png</key>
            <key type="filename">tiles/gems/common/gem_common_073.png</key>
            <key type="filename">tiles/gems/common/gem_common_074.png</key>
            <key type="filename">tiles/gems/common/gem_common_075.png</key>
            <key type="filename">tiles/gems/common/gem_common_076.png</key>
            <key type="filename">tiles/gems/common/gem_common_077.png</key>
            <key type="filename">tiles/gems/common/gem_common_078.png</key>
            <key type="filename">tiles/gems/common/gem_common_079.png</key>
            <key type="filename">tiles/gems/common/gem_common_080.png</key>
            <key type="filename">tiles/gems/common/gem_common_081.png</key>
            <key type="filename">tiles/gems/common/gem_common_082.png</key>
            <key type="filename">tiles/gems/common/gem_common_083.png</key>
            <key type="filename">tiles/gems/common/gem_common_084.png</key>
            <key type="filename">tiles/gems/common/gem_common_085.png</key>
            <key type="filename">tiles/gems/common/gem_common_086.png</key>
            <key type="filename">tiles/gems/common/gem_common_087.png</key>
            <key type="filename">tiles/gems/common/gem_common_088.png</key>
            <key type="filename">tiles/gems/common/gem_common_089.png</key>
            <key type="filename">tiles/gems/common/gem_common_090.png</key>
            <key type="filename">tiles/gems/common/gem_common_091.png</key>
            <key type="filename">tiles/gems/common/gem_common_092.png</key>
            <key type="filename">tiles/gems/common/gem_common_093.png</key>
            <key type="filename">tiles/gems/rare/gem_rare_000.png</key>
            <key type="filename">tiles/gems/rare/gem_rare_001.png</key>
            <key type="filename">tiles/gems/rare/gem_rare_002.png</key>
            <key type="filename">tiles/gems/rare/gem_rare_003.png</key>
            <key type="filename">tiles/gems/rare/gem_rare_004.png</key>
            <key type="filename">tiles/gems/rare/gem_rare_005.png</key>
            <key type="filename">tiles/gems/rare/gem_rare_006.png</key>
            <key type="filename">tiles/gems/rare/gem_rare_007.png</key>
            <key type="filename">tiles/gems/rare/gem_rare_008.png</key>
            <key type="filename">tiles/gems/rare/gem_rare_009.png</key>
            <key type="filename">tiles/gems/rare/gem_rare_010.png</key>
            <key type="filename">tiles/gems/rare/gem_rare_011.png</key>
            <key type="filename">tiles/gems/rare/gem_rare_012.png</key>
            <key type="filename">tiles/gems/rare/gem_rare_013.png</key>
            <key type="filename">tiles/gems/rare/gem_rare_014.png</key>
            <key type="filename">tiles/gems/rare/gem_rare_015.png</key>
            <key type="filename">tiles/gems/rare/gem_rare_016.png</key>
            <key type="filename">tiles/gems/rare/gem_rare_017.png</key>
            <key type="filename">tiles/gems/rare/gem_rare_018.png</key>
            <key type="filename">tiles/gems/rare/gem_rare_019.png</key>
            <key type="filename">tiles/gems/rare/gem_rare_020.png</key>
            <key type="filename">tiles/gems/rare/gem_rare_021.png</key>
            <key type="filename">tiles/gems/rare/gem_rare_022.png</key>
            <key type="filename">tiles/gems/rare/gem_rare_023.png</key>
            <key type="filename">tiles/gems/rare/gem_rare_024.png</key>
            <key type="filename">tiles/gems/rare/gem_rare_025.png</key>
            <key type="filename">tiles/gems/rare/gem_rare_026.png</key>
            <key type="filename">tiles/gems/rare/gem_rare_027.png</key>
            <key type="filename">tiles/gems/rare/gem_rare_028.png</key>
            <key type="filename">tiles/gems/rare/gem_rare_029.png</key>
            <key type="filename">tiles/gems/unique/gem_unique_000.png</key>
            <key type="filename">tiles/gems/unique/gem_unique_001.png</key>
            <key type="filename">tiles/gems/unique/gem_unique_002.png</key>
            <key type="filename">tiles/gems/unique/gem_unique_003.png</key>
            <key type="filename">tiles/gems/unique/gem_unique_004.png</key>
            <key type="filename">tiles/gems/unique/gem_unique_005.png</key>
            <key type="filename">tiles/gems/unique/gem_unique_006.png</key>
            <key type="filename">tiles/gems/unique/gem_unique_007.png</key>
            <key type="filename">tiles/gems/unique/gem_unique_008.png</key>
            <key type="filename">tiles/gems/unique/gem_unique_009.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,16,32,32</rect>
                <key>scale9Paddings</key>
                <rect>16,16,32,32</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">tiles/nitrate.png</key>
            <key type="filename">tiles/sulfur.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,32,64,64</rect>
                <key>scale9Paddings</key>
                <rect>32,32,64,64</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">tiles/nitrate/rock_nitrate_close_ne_00.png</key>
            <key type="filename">tiles/nitrate/rock_nitrate_close_nw_00.png</key>
            <key type="filename">tiles/nitrate/rock_nitrate_close_se_L0_00.png</key>
            <key type="filename">tiles/nitrate/rock_nitrate_close_se_L1_00.png</key>
            <key type="filename">tiles/nitrate/rock_nitrate_close_se_L2_00.png</key>
            <key type="filename">tiles/nitrate/rock_nitrate_close_sw_L0_00.png</key>
            <key type="filename">tiles/nitrate/rock_nitrate_close_sw_L1_00.png</key>
            <key type="filename">tiles/nitrate/rock_nitrate_close_sw_L2_00.png</key>
            <key type="filename">tiles/nitrate/rock_nitrate_e_00.png</key>
            <key type="filename">tiles/nitrate/rock_nitrate_e_01.png</key>
            <key type="filename">tiles/nitrate/rock_nitrate_e_02.png</key>
            <key type="filename">tiles/nitrate/rock_nitrate_e_03.png</key>
            <key type="filename">tiles/nitrate/rock_nitrate_e_04.png</key>
            <key type="filename">tiles/nitrate/rock_nitrate_n_00.png</key>
            <key type="filename">tiles/nitrate/rock_nitrate_n_01.png</key>
            <key type="filename">tiles/nitrate/rock_nitrate_n_02.png</key>
            <key type="filename">tiles/nitrate/rock_nitrate_n_03.png</key>
            <key type="filename">tiles/nitrate/rock_nitrate_open_ne_00.png</key>
            <key type="filename">tiles/nitrate/rock_nitrate_open_nw_00.png</key>
            <key type="filename">tiles/nitrate/rock_nitrate_open_se_L0_00.png</key>
            <key type="filename">tiles/nitrate/rock_nitrate_open_se_L1_00.png</key>
            <key type="filename">tiles/nitrate/rock_nitrate_open_se_L2_00.png</key>
            <key type="filename">tiles/nitrate/rock_nitrate_open_sw_L0_00.png</key>
            <key type="filename">tiles/nitrate/rock_nitrate_open_sw_L1_00.png</key>
            <key type="filename">tiles/nitrate/rock_nitrate_open_sw_L2_00.png</key>
            <key type="filename">tiles/nitrate/rock_nitrate_s_L0_00.png</key>
            <key type="filename">tiles/nitrate/rock_nitrate_s_L0_01.png</key>
            <key type="filename">tiles/nitrate/rock_nitrate_s_L0_02.png</key>
            <key type="filename">tiles/nitrate/rock_nitrate_s_L0_03.png</key>
            <key type="filename">tiles/nitrate/rock_nitrate_s_L1_00.png</key>
            <key type="filename">tiles/nitrate/rock_nitrate_s_L1_01.png</key>
            <key type="filename">tiles/nitrate/rock_nitrate_s_L1_02.png</key>
            <key type="filename">tiles/nitrate/rock_nitrate_s_L1_03.png</key>
            <key type="filename">tiles/nitrate/rock_nitrate_s_L2_00.png</key>
            <key type="filename">tiles/nitrate/rock_nitrate_s_L2_01.png</key>
            <key type="filename">tiles/nitrate/rock_nitrate_s_L2_02.png</key>
            <key type="filename">tiles/nitrate/rock_nitrate_s_L2_03.png</key>
            <key type="filename">tiles/nitrate/rock_nitrate_w_00.png</key>
            <key type="filename">tiles/nitrate/rock_nitrate_w_01.png</key>
            <key type="filename">tiles/nitrate/rock_nitrate_w_02.png</key>
            <key type="filename">tiles/nitrate/rock_nitrate_w_03.png</key>
            <key type="filename">tiles/nitrate/rock_nitrate_w_04.png</key>
            <key type="filename">tiles/rock/rock_black_00.png</key>
            <key type="filename">tiles/rock/rock_close_ne_00.png</key>
            <key type="filename">tiles/rock/rock_close_nw_00.png</key>
            <key type="filename">tiles/rock/rock_close_se_L0_00.png</key>
            <key type="filename">tiles/rock/rock_close_se_L1_00.png</key>
            <key type="filename">tiles/rock/rock_close_se_L2_00.png</key>
            <key type="filename">tiles/rock/rock_close_sw_L0_00.png</key>
            <key type="filename">tiles/rock/rock_close_sw_L1_00.png</key>
            <key type="filename">tiles/rock/rock_close_sw_L2_00.png</key>
            <key type="filename">tiles/rock/rock_e_00.png</key>
            <key type="filename">tiles/rock/rock_e_01.png</key>
            <key type="filename">tiles/rock/rock_e_02.png</key>
            <key type="filename">tiles/rock/rock_e_03.png</key>
            <key type="filename">tiles/rock/rock_e_04.png</key>
            <key type="filename">tiles/rock/rock_fill_00.png</key>
            <key type="filename">tiles/rock/rock_fill_01.png</key>
            <key type="filename">tiles/rock/rock_fill_02.png</key>
            <key type="filename">tiles/rock/rock_fill_03.png</key>
            <key type="filename">tiles/rock/rock_fill_04.png</key>
            <key type="filename">tiles/rock/rock_fill_05.png</key>
            <key type="filename">tiles/rock/rock_fill_06.png</key>
            <key type="filename">tiles/rock/rock_fill_07.png</key>
            <key type="filename">tiles/rock/rock_fill_08.png</key>
            <key type="filename">tiles/rock/rock_fill_09.png</key>
            <key type="filename">tiles/rock/rock_fill_10.png</key>
            <key type="filename">tiles/rock/rock_fill_11.png</key>
            <key type="filename">tiles/rock/rock_n_00.png</key>
            <key type="filename">tiles/rock/rock_n_01.png</key>
            <key type="filename">tiles/rock/rock_n_02.png</key>
            <key type="filename">tiles/rock/rock_n_03.png</key>
            <key type="filename">tiles/rock/rock_open_ne_00.png</key>
            <key type="filename">tiles/rock/rock_open_nw_00.png</key>
            <key type="filename">tiles/rock/rock_open_se_L0_00.png</key>
            <key type="filename">tiles/rock/rock_open_se_L1_00.png</key>
            <key type="filename">tiles/rock/rock_open_se_L2_00.png</key>
            <key type="filename">tiles/rock/rock_open_sw_L0_00.png</key>
            <key type="filename">tiles/rock/rock_open_sw_L1_00.png</key>
            <key type="filename">tiles/rock/rock_open_sw_L2_00.png</key>
            <key type="filename">tiles/rock/rock_s_L0_00.png</key>
            <key type="filename">tiles/rock/rock_s_L0_01.png</key>
            <key type="filename">tiles/rock/rock_s_L0_02.png</key>
            <key type="filename">tiles/rock/rock_s_L0_03.png</key>
            <key type="filename">tiles/rock/rock_s_L1_00.png</key>
            <key type="filename">tiles/rock/rock_s_L1_01.png</key>
            <key type="filename">tiles/rock/rock_s_L1_02.png</key>
            <key type="filename">tiles/rock/rock_s_L1_03.png</key>
            <key type="filename">tiles/rock/rock_s_L2_00.png</key>
            <key type="filename">tiles/rock/rock_s_L2_01.png</key>
            <key type="filename">tiles/rock/rock_s_L2_02.png</key>
            <key type="filename">tiles/rock/rock_s_L2_03.png</key>
            <key type="filename">tiles/rock/rock_w_00.png</key>
            <key type="filename">tiles/rock/rock_w_01.png</key>
            <key type="filename">tiles/rock/rock_w_02.png</key>
            <key type="filename">tiles/rock/rock_w_03.png</key>
            <key type="filename">tiles/rock/rock_w_04.png</key>
            <key type="filename">tiles/soil_dirt_00.png</key>
            <key type="filename">tiles/soil_dirt_01.png</key>
            <key type="filename">tiles/soil_dirt_02.png</key>
            <key type="filename">tiles/soil_dirt_03.png</key>
            <key type="filename">tiles/soil_grass_00.png</key>
            <key type="filename">tiles/soil_grass_01.png</key>
            <key type="filename">tiles/soil_grass_02.png</key>
            <key type="filename">tiles/soil_grass_03.png</key>
            <key type="filename">tiles/soil_grass_04.png</key>
            <key type="filename">tiles/soil_grass_05.png</key>
            <key type="filename">tiles/soil_grass_06.png</key>
            <key type="filename">tiles/soil_grass_07.png</key>
            <key type="filename">tiles/soil_grass_08.png</key>
            <key type="filename">tiles/soil_grass_09.png</key>
            <key type="filename">tiles/soil_grass_10.png</key>
            <key type="filename">tiles/soil_grass_11.png</key>
            <key type="filename">tiles/soil_grass_12.png</key>
            <key type="filename">tiles/soil_grass_13.png</key>
            <key type="filename">tiles/soil_rock_00.png</key>
            <key type="filename">tiles/soil_rock_01.png</key>
            <key type="filename">tiles/soil_rock_02.png</key>
            <key type="filename">tiles/soil_rock_03.png</key>
            <key type="filename">tiles/soil_rock_04.png</key>
            <key type="filename">tiles/soil_rock_05.png</key>
            <key type="filename">tiles/soil_rock_06.png</key>
            <key type="filename">tiles/soil_rock_07.png</key>
            <key type="filename">tiles/soil_rock_08.png</key>
            <key type="filename">tiles/soil_rock_09.png</key>
            <key type="filename">tiles/soil_rock_10.png</key>
            <key type="filename">tiles/soil_rock_11.png</key>
            <key type="filename">tiles/sulfur/rock_sulfur_close_ne_00.png</key>
            <key type="filename">tiles/sulfur/rock_sulfur_close_nw_00.png</key>
            <key type="filename">tiles/sulfur/rock_sulfur_close_se_L0_00.png</key>
            <key type="filename">tiles/sulfur/rock_sulfur_close_se_L1_00.png</key>
            <key type="filename">tiles/sulfur/rock_sulfur_close_se_L2_00.png</key>
            <key type="filename">tiles/sulfur/rock_sulfur_close_sw_L0_00.png</key>
            <key type="filename">tiles/sulfur/rock_sulfur_close_sw_L1_00.png</key>
            <key type="filename">tiles/sulfur/rock_sulfur_close_sw_L2_00.png</key>
            <key type="filename">tiles/sulfur/rock_sulfur_e_00.png</key>
            <key type="filename">tiles/sulfur/rock_sulfur_e_01.png</key>
            <key type="filename">tiles/sulfur/rock_sulfur_e_02.png</key>
            <key type="filename">tiles/sulfur/rock_sulfur_e_03.png</key>
            <key type="filename">tiles/sulfur/rock_sulfur_e_04.png</key>
            <key type="filename">tiles/sulfur/rock_sulfur_n_00.png</key>
            <key type="filename">tiles/sulfur/rock_sulfur_n_01.png</key>
            <key type="filename">tiles/sulfur/rock_sulfur_n_02.png</key>
            <key type="filename">tiles/sulfur/rock_sulfur_n_03.png</key>
            <key type="filename">tiles/sulfur/rock_sulfur_open_ne_00.png</key>
            <key type="filename">tiles/sulfur/rock_sulfur_open_nw_00.png</key>
            <key type="filename">tiles/sulfur/rock_sulfur_open_se_L0_00.png</key>
            <key type="filename">tiles/sulfur/rock_sulfur_open_se_L1_00.png</key>
            <key type="filename">tiles/sulfur/rock_sulfur_open_se_L2_00.png</key>
            <key type="filename">tiles/sulfur/rock_sulfur_open_sw_L0_00.png</key>
            <key type="filename">tiles/sulfur/rock_sulfur_open_sw_L1_00.png</key>
            <key type="filename">tiles/sulfur/rock_sulfur_open_sw_L2_00.png</key>
            <key type="filename">tiles/sulfur/rock_sulfur_s_L0_00.png</key>
            <key type="filename">tiles/sulfur/rock_sulfur_s_L0_01.png</key>
            <key type="filename">tiles/sulfur/rock_sulfur_s_L0_02.png</key>
            <key type="filename">tiles/sulfur/rock_sulfur_s_L0_03.png</key>
            <key type="filename">tiles/sulfur/rock_sulfur_s_L1_00.png</key>
            <key type="filename">tiles/sulfur/rock_sulfur_s_L1_01.png</key>
            <key type="filename">tiles/sulfur/rock_sulfur_s_L1_02.png</key>
            <key type="filename">tiles/sulfur/rock_sulfur_s_L1_03.png</key>
            <key type="filename">tiles/sulfur/rock_sulfur_s_L2_00.png</key>
            <key type="filename">tiles/sulfur/rock_sulfur_s_L2_01.png</key>
            <key type="filename">tiles/sulfur/rock_sulfur_s_L2_02.png</key>
            <key type="filename">tiles/sulfur/rock_sulfur_s_L2_03.png</key>
            <key type="filename">tiles/sulfur/rock_sulfur_w_00.png</key>
            <key type="filename">tiles/sulfur/rock_sulfur_w_01.png</key>
            <key type="filename">tiles/sulfur/rock_sulfur_w_02.png</key>
            <key type="filename">tiles/sulfur/rock_sulfur_w_03.png</key>
            <key type="filename">tiles/sulfur/rock_sulfur_w_04.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,8,16,16</rect>
                <key>scale9Paddings</key>
                <rect>8,8,16,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>tiles/button_off.png</filename>
            <filename>tiles/button_on.png</filename>
            <filename>tiles/dynomite_off.png</filename>
            <filename>tiles/dynomite_on.png</filename>
            <filename>tiles/nitrate.png</filename>
            <filename>tiles/soil_dirt_00.png</filename>
            <filename>tiles/soil_dirt_01.png</filename>
            <filename>tiles/soil_dirt_02.png</filename>
            <filename>tiles/soil_dirt_03.png</filename>
            <filename>tiles/soil_grass_00.png</filename>
            <filename>tiles/soil_grass_01.png</filename>
            <filename>tiles/soil_grass_02.png</filename>
            <filename>tiles/soil_grass_03.png</filename>
            <filename>tiles/soil_grass_04.png</filename>
            <filename>tiles/soil_grass_05.png</filename>
            <filename>tiles/soil_grass_06.png</filename>
            <filename>tiles/soil_grass_07.png</filename>
            <filename>tiles/soil_grass_08.png</filename>
            <filename>tiles/soil_grass_09.png</filename>
            <filename>tiles/soil_grass_10.png</filename>
            <filename>tiles/soil_grass_11.png</filename>
            <filename>tiles/soil_grass_12.png</filename>
            <filename>tiles/soil_grass_13.png</filename>
            <filename>tiles/soil_rock_00.png</filename>
            <filename>tiles/soil_rock_01.png</filename>
            <filename>tiles/soil_rock_02.png</filename>
            <filename>tiles/soil_rock_03.png</filename>
            <filename>tiles/soil_rock_04.png</filename>
            <filename>tiles/soil_rock_05.png</filename>
            <filename>tiles/soil_rock_06.png</filename>
            <filename>tiles/soil_rock_07.png</filename>
            <filename>tiles/soil_rock_08.png</filename>
            <filename>tiles/soil_rock_09.png</filename>
            <filename>tiles/soil_rock_10.png</filename>
            <filename>tiles/soil_rock_11.png</filename>
            <filename>tiles/sulfur.png</filename>
            <filename>tiles/sulfur/rock_sulfur_close_ne_00.png</filename>
            <filename>tiles/sulfur/rock_sulfur_close_nw_00.png</filename>
            <filename>tiles/sulfur/rock_sulfur_close_se_L0_00.png</filename>
            <filename>tiles/sulfur/rock_sulfur_close_se_L1_00.png</filename>
            <filename>tiles/sulfur/rock_sulfur_close_se_L2_00.png</filename>
            <filename>tiles/sulfur/rock_sulfur_close_sw_L0_00.png</filename>
            <filename>tiles/sulfur/rock_sulfur_close_sw_L1_00.png</filename>
            <filename>tiles/sulfur/rock_sulfur_close_sw_L2_00.png</filename>
            <filename>tiles/sulfur/rock_sulfur_e_00.png</filename>
            <filename>tiles/sulfur/rock_sulfur_e_01.png</filename>
            <filename>tiles/sulfur/rock_sulfur_e_02.png</filename>
            <filename>tiles/sulfur/rock_sulfur_e_03.png</filename>
            <filename>tiles/sulfur/rock_sulfur_e_04.png</filename>
            <filename>tiles/sulfur/rock_sulfur_n_00.png</filename>
            <filename>tiles/sulfur/rock_sulfur_n_01.png</filename>
            <filename>tiles/sulfur/rock_sulfur_n_02.png</filename>
            <filename>tiles/sulfur/rock_sulfur_n_03.png</filename>
            <filename>tiles/sulfur/rock_sulfur_open_ne_00.png</filename>
            <filename>tiles/sulfur/rock_sulfur_open_nw_00.png</filename>
            <filename>tiles/sulfur/rock_sulfur_open_se_L0_00.png</filename>
            <filename>tiles/sulfur/rock_sulfur_open_se_L1_00.png</filename>
            <filename>tiles/sulfur/rock_sulfur_open_se_L2_00.png</filename>
            <filename>tiles/sulfur/rock_sulfur_open_sw_L0_00.png</filename>
            <filename>tiles/sulfur/rock_sulfur_open_sw_L1_00.png</filename>
            <filename>tiles/sulfur/rock_sulfur_open_sw_L2_00.png</filename>
            <filename>tiles/sulfur/rock_sulfur_s_L0_00.png</filename>
            <filename>tiles/sulfur/rock_sulfur_s_L0_01.png</filename>
            <filename>tiles/sulfur/rock_sulfur_s_L0_02.png</filename>
            <filename>tiles/sulfur/rock_sulfur_s_L0_03.png</filename>
            <filename>tiles/sulfur/rock_sulfur_s_L1_00.png</filename>
            <filename>tiles/sulfur/rock_sulfur_s_L1_01.png</filename>
            <filename>tiles/sulfur/rock_sulfur_s_L1_02.png</filename>
            <filename>tiles/sulfur/rock_sulfur_s_L1_03.png</filename>
            <filename>tiles/sulfur/rock_sulfur_s_L2_00.png</filename>
            <filename>tiles/sulfur/rock_sulfur_s_L2_01.png</filename>
            <filename>tiles/sulfur/rock_sulfur_s_L2_02.png</filename>
            <filename>tiles/sulfur/rock_sulfur_s_L2_03.png</filename>
            <filename>tiles/sulfur/rock_sulfur_w_00.png</filename>
            <filename>tiles/sulfur/rock_sulfur_w_01.png</filename>
            <filename>tiles/sulfur/rock_sulfur_w_02.png</filename>
            <filename>tiles/sulfur/rock_sulfur_w_03.png</filename>
            <filename>tiles/sulfur/rock_sulfur_w_04.png</filename>
            <filename>tiles/rock/rock_black_00.png</filename>
            <filename>tiles/rock/rock_close_ne_00.png</filename>
            <filename>tiles/rock/rock_close_nw_00.png</filename>
            <filename>tiles/rock/rock_close_se_L0_00.png</filename>
            <filename>tiles/rock/rock_close_se_L1_00.png</filename>
            <filename>tiles/rock/rock_close_se_L2_00.png</filename>
            <filename>tiles/rock/rock_close_sw_L0_00.png</filename>
            <filename>tiles/rock/rock_close_sw_L1_00.png</filename>
            <filename>tiles/rock/rock_close_sw_L2_00.png</filename>
            <filename>tiles/rock/rock_e_00.png</filename>
            <filename>tiles/rock/rock_e_01.png</filename>
            <filename>tiles/rock/rock_e_02.png</filename>
            <filename>tiles/rock/rock_e_03.png</filename>
            <filename>tiles/rock/rock_e_04.png</filename>
            <filename>tiles/rock/rock_fill_00.png</filename>
            <filename>tiles/rock/rock_fill_01.png</filename>
            <filename>tiles/rock/rock_fill_02.png</filename>
            <filename>tiles/rock/rock_fill_03.png</filename>
            <filename>tiles/rock/rock_fill_04.png</filename>
            <filename>tiles/rock/rock_fill_05.png</filename>
            <filename>tiles/rock/rock_fill_06.png</filename>
            <filename>tiles/rock/rock_fill_07.png</filename>
            <filename>tiles/rock/rock_fill_08.png</filename>
            <filename>tiles/rock/rock_fill_09.png</filename>
            <filename>tiles/rock/rock_fill_10.png</filename>
            <filename>tiles/rock/rock_fill_11.png</filename>
            <filename>tiles/rock/rock_n_00.png</filename>
            <filename>tiles/rock/rock_n_01.png</filename>
            <filename>tiles/rock/rock_n_02.png</filename>
            <filename>tiles/rock/rock_n_03.png</filename>
            <filename>tiles/rock/rock_open_ne_00.png</filename>
            <filename>tiles/rock/rock_open_nw_00.png</filename>
            <filename>tiles/rock/rock_open_se_L0_00.png</filename>
            <filename>tiles/rock/rock_open_se_L1_00.png</filename>
            <filename>tiles/rock/rock_open_se_L2_00.png</filename>
            <filename>tiles/rock/rock_open_sw_L0_00.png</filename>
            <filename>tiles/rock/rock_open_sw_L1_00.png</filename>
            <filename>tiles/rock/rock_open_sw_L2_00.png</filename>
            <filename>tiles/rock/rock_s_L0_00.png</filename>
            <filename>tiles/rock/rock_s_L0_01.png</filename>
            <filename>tiles/rock/rock_s_L0_02.png</filename>
            <filename>tiles/rock/rock_s_L0_03.png</filename>
            <filename>tiles/rock/rock_s_L1_00.png</filename>
            <filename>tiles/rock/rock_s_L1_01.png</filename>
            <filename>tiles/rock/rock_s_L1_02.png</filename>
            <filename>tiles/rock/rock_s_L1_03.png</filename>
            <filename>tiles/rock/rock_s_L2_00.png</filename>
            <filename>tiles/rock/rock_s_L2_01.png</filename>
            <filename>tiles/rock/rock_s_L2_02.png</filename>
            <filename>tiles/rock/rock_s_L2_03.png</filename>
            <filename>tiles/rock/rock_w_00.png</filename>
            <filename>tiles/rock/rock_w_01.png</filename>
            <filename>tiles/rock/rock_w_02.png</filename>
            <filename>tiles/rock/rock_w_03.png</filename>
            <filename>tiles/rock/rock_w_04.png</filename>
            <filename>tiles/nitrate/rock_nitrate_close_ne_00.png</filename>
            <filename>tiles/nitrate/rock_nitrate_close_nw_00.png</filename>
            <filename>tiles/nitrate/rock_nitrate_close_se_L0_00.png</filename>
            <filename>tiles/nitrate/rock_nitrate_close_se_L1_00.png</filename>
            <filename>tiles/nitrate/rock_nitrate_close_se_L2_00.png</filename>
            <filename>tiles/nitrate/rock_nitrate_close_sw_L0_00.png</filename>
            <filename>tiles/nitrate/rock_nitrate_close_sw_L1_00.png</filename>
            <filename>tiles/nitrate/rock_nitrate_close_sw_L2_00.png</filename>
            <filename>tiles/nitrate/rock_nitrate_e_00.png</filename>
            <filename>tiles/nitrate/rock_nitrate_e_01.png</filename>
            <filename>tiles/nitrate/rock_nitrate_e_02.png</filename>
            <filename>tiles/nitrate/rock_nitrate_e_03.png</filename>
            <filename>tiles/nitrate/rock_nitrate_e_04.png</filename>
            <filename>tiles/nitrate/rock_nitrate_n_00.png</filename>
            <filename>tiles/nitrate/rock_nitrate_n_01.png</filename>
            <filename>tiles/nitrate/rock_nitrate_n_02.png</filename>
            <filename>tiles/nitrate/rock_nitrate_n_03.png</filename>
            <filename>tiles/nitrate/rock_nitrate_open_ne_00.png</filename>
            <filename>tiles/nitrate/rock_nitrate_open_nw_00.png</filename>
            <filename>tiles/nitrate/rock_nitrate_open_se_L0_00.png</filename>
            <filename>tiles/nitrate/rock_nitrate_open_se_L1_00.png</filename>
            <filename>tiles/nitrate/rock_nitrate_open_se_L2_00.png</filename>
            <filename>tiles/nitrate/rock_nitrate_open_sw_L0_00.png</filename>
            <filename>tiles/nitrate/rock_nitrate_open_sw_L1_00.png</filename>
            <filename>tiles/nitrate/rock_nitrate_open_sw_L2_00.png</filename>
            <filename>tiles/nitrate/rock_nitrate_s_L0_00.png</filename>
            <filename>tiles/nitrate/rock_nitrate_s_L0_01.png</filename>
            <filename>tiles/nitrate/rock_nitrate_s_L0_02.png</filename>
            <filename>tiles/nitrate/rock_nitrate_s_L0_03.png</filename>
            <filename>tiles/nitrate/rock_nitrate_s_L1_00.png</filename>
            <filename>tiles/nitrate/rock_nitrate_s_L1_01.png</filename>
            <filename>tiles/nitrate/rock_nitrate_s_L1_02.png</filename>
            <filename>tiles/nitrate/rock_nitrate_s_L1_03.png</filename>
            <filename>tiles/nitrate/rock_nitrate_s_L2_00.png</filename>
            <filename>tiles/nitrate/rock_nitrate_s_L2_01.png</filename>
            <filename>tiles/nitrate/rock_nitrate_s_L2_02.png</filename>
            <filename>tiles/nitrate/rock_nitrate_s_L2_03.png</filename>
            <filename>tiles/nitrate/rock_nitrate_w_00.png</filename>
            <filename>tiles/nitrate/rock_nitrate_w_01.png</filename>
            <filename>tiles/nitrate/rock_nitrate_w_02.png</filename>
            <filename>tiles/nitrate/rock_nitrate_w_03.png</filename>
            <filename>tiles/nitrate/rock_nitrate_w_04.png</filename>
            <filename>tiles/explosion/explosion_001.png</filename>
            <filename>tiles/explosion/explosion_002.png</filename>
            <filename>tiles/explosion/explosion_003.png</filename>
            <filename>tiles/explosion/explosion_004.png</filename>
            <filename>tiles/explosion/explosion_005.png</filename>
            <filename>tiles/explosion/explosion_006.png</filename>
            <filename>tiles/explosion/explosion_007.png</filename>
            <filename>tiles/explosion/explosion_008.png</filename>
            <filename>tiles/explosion/explosion_009.png</filename>
            <filename>tiles/explosion/explosion_010.png</filename>
            <filename>tiles/explosion/explosion_011.png</filename>
            <filename>tiles/explosion/explosion_012.png</filename>
            <filename>tiles/explosion/explosion_013.png</filename>
            <filename>tiles/explosion/explosion_014.png</filename>
            <filename>tiles/explosion/explosion_015.png</filename>
            <filename>tiles/explosion/explosion_016.png</filename>
            <filename>tiles/explosion/explosion_017.png</filename>
            <filename>tiles/explosion/explosion_018.png</filename>
            <filename>tiles/explosion/explosion_019.png</filename>
            <filename>tiles/explosion/explosion_020.png</filename>
            <filename>tiles/explosion/explosion_021.png</filename>
            <filename>tiles/explosion/explosion_022.png</filename>
            <filename>tiles/explosion/explosion_023.png</filename>
            <filename>tiles/explosion/explosion_024.png</filename>
            <filename>tiles/explosion/explosion_025.png</filename>
            <filename>tiles/explosion/explosion_026.png</filename>
            <filename>tiles/explosion/explosion_027.png</filename>
            <filename>tiles/explosion/explosion_028.png</filename>
            <filename>tiles/explosion/explosion_029.png</filename>
            <filename>tiles/explosion/explosion_030.png</filename>
            <filename>tiles/explosion/explosion_031.png</filename>
            <filename>tiles/explosion/explosion_032.png</filename>
            <filename>tiles/explosion/explosion_033.png</filename>
            <filename>tiles/explosion/explosion_034.png</filename>
            <filename>tiles/explosion/explosion_035.png</filename>
            <filename>tiles/explosion/explosion_036.png</filename>
            <filename>tiles/explosion/explosion_037.png</filename>
            <filename>tiles/explosion/explosion_038.png</filename>
            <filename>tiles/explosion/explosion_039.png</filename>
            <filename>tiles/explosion/explosion_040.png</filename>
            <filename>tiles/explosion/explosion_041.png</filename>
            <filename>tiles/explosion/explosion_042.png</filename>
            <filename>tiles/explosion/explosion_043.png</filename>
            <filename>tiles/explosion/explosion_044.png</filename>
            <filename>tiles/explosion/explosion_045.png</filename>
            <filename>tiles/explosion/explosion_046.png</filename>
            <filename>tiles/explosion/explosion_047.png</filename>
            <filename>tiles/explosion/explosion_048.png</filename>
            <filename>tiles/explosion/explosion_049.png</filename>
            <filename>tiles/explosion/explosion_050.png</filename>
            <filename>tiles/explosion/explosion_051.png</filename>
            <filename>tiles/explosion/explosion_052.png</filename>
            <filename>tiles/explosion/explosion_053.png</filename>
            <filename>tiles/explosion/explosion_054.png</filename>
            <filename>tiles/explosion/explosion_055.png</filename>
            <filename>tiles/explosion/explosion_056.png</filename>
            <filename>tiles/explosion/explosion_057.png</filename>
            <filename>tiles/explosion/explosion_058.png</filename>
            <filename>tiles/explosion/explosion_059.png</filename>
            <filename>tiles/explosion/explosion_060.png</filename>
            <filename>tiles/explosion/explosion_061.png</filename>
            <filename>tiles/explosion/explosion_062.png</filename>
            <filename>tiles/explosion/explosion_063.png</filename>
            <filename>tiles/explosion/explosion_064.png</filename>
            <filename>tiles/gems/unique/gem_unique_000.png</filename>
            <filename>tiles/gems/unique/gem_unique_001.png</filename>
            <filename>tiles/gems/unique/gem_unique_002.png</filename>
            <filename>tiles/gems/unique/gem_unique_003.png</filename>
            <filename>tiles/gems/unique/gem_unique_004.png</filename>
            <filename>tiles/gems/unique/gem_unique_005.png</filename>
            <filename>tiles/gems/unique/gem_unique_006.png</filename>
            <filename>tiles/gems/unique/gem_unique_007.png</filename>
            <filename>tiles/gems/unique/gem_unique_008.png</filename>
            <filename>tiles/gems/unique/gem_unique_009.png</filename>
            <filename>tiles/gems/rare/gem_rare_000.png</filename>
            <filename>tiles/gems/rare/gem_rare_001.png</filename>
            <filename>tiles/gems/rare/gem_rare_002.png</filename>
            <filename>tiles/gems/rare/gem_rare_003.png</filename>
            <filename>tiles/gems/rare/gem_rare_004.png</filename>
            <filename>tiles/gems/rare/gem_rare_005.png</filename>
            <filename>tiles/gems/rare/gem_rare_006.png</filename>
            <filename>tiles/gems/rare/gem_rare_007.png</filename>
            <filename>tiles/gems/rare/gem_rare_008.png</filename>
            <filename>tiles/gems/rare/gem_rare_009.png</filename>
            <filename>tiles/gems/rare/gem_rare_010.png</filename>
            <filename>tiles/gems/rare/gem_rare_011.png</filename>
            <filename>tiles/gems/rare/gem_rare_012.png</filename>
            <filename>tiles/gems/rare/gem_rare_013.png</filename>
            <filename>tiles/gems/rare/gem_rare_014.png</filename>
            <filename>tiles/gems/rare/gem_rare_015.png</filename>
            <filename>tiles/gems/rare/gem_rare_016.png</filename>
            <filename>tiles/gems/rare/gem_rare_017.png</filename>
            <filename>tiles/gems/rare/gem_rare_018.png</filename>
            <filename>tiles/gems/rare/gem_rare_019.png</filename>
            <filename>tiles/gems/rare/gem_rare_020.png</filename>
            <filename>tiles/gems/rare/gem_rare_021.png</filename>
            <filename>tiles/gems/rare/gem_rare_022.png</filename>
            <filename>tiles/gems/rare/gem_rare_023.png</filename>
            <filename>tiles/gems/rare/gem_rare_024.png</filename>
            <filename>tiles/gems/rare/gem_rare_025.png</filename>
            <filename>tiles/gems/rare/gem_rare_026.png</filename>
            <filename>tiles/gems/rare/gem_rare_027.png</filename>
            <filename>tiles/gems/rare/gem_rare_028.png</filename>
            <filename>tiles/gems/rare/gem_rare_029.png</filename>
            <filename>tiles/gems/common/gem_common_000.png</filename>
            <filename>tiles/gems/common/gem_common_001.png</filename>
            <filename>tiles/gems/common/gem_common_002.png</filename>
            <filename>tiles/gems/common/gem_common_003.png</filename>
            <filename>tiles/gems/common/gem_common_004.png</filename>
            <filename>tiles/gems/common/gem_common_005.png</filename>
            <filename>tiles/gems/common/gem_common_006.png</filename>
            <filename>tiles/gems/common/gem_common_007.png</filename>
            <filename>tiles/gems/common/gem_common_008.png</filename>
            <filename>tiles/gems/common/gem_common_009.png</filename>
            <filename>tiles/gems/common/gem_common_010.png</filename>
            <filename>tiles/gems/common/gem_common_011.png</filename>
            <filename>tiles/gems/common/gem_common_012.png</filename>
            <filename>tiles/gems/common/gem_common_013.png</filename>
            <filename>tiles/gems/common/gem_common_014.png</filename>
            <filename>tiles/gems/common/gem_common_015.png</filename>
            <filename>tiles/gems/common/gem_common_016.png</filename>
            <filename>tiles/gems/common/gem_common_017.png</filename>
            <filename>tiles/gems/common/gem_common_018.png</filename>
            <filename>tiles/gems/common/gem_common_019.png</filename>
            <filename>tiles/gems/common/gem_common_020.png</filename>
            <filename>tiles/gems/common/gem_common_021.png</filename>
            <filename>tiles/gems/common/gem_common_022.png</filename>
            <filename>tiles/gems/common/gem_common_023.png</filename>
            <filename>tiles/gems/common/gem_common_024.png</filename>
            <filename>tiles/gems/common/gem_common_025.png</filename>
            <filename>tiles/gems/common/gem_common_026.png</filename>
            <filename>tiles/gems/common/gem_common_027.png</filename>
            <filename>tiles/gems/common/gem_common_028.png</filename>
            <filename>tiles/gems/common/gem_common_029.png</filename>
            <filename>tiles/gems/common/gem_common_030.png</filename>
            <filename>tiles/gems/common/gem_common_031.png</filename>
            <filename>tiles/gems/common/gem_common_032.png</filename>
            <filename>tiles/gems/common/gem_common_033.png</filename>
            <filename>tiles/gems/common/gem_common_034.png</filename>
            <filename>tiles/gems/common/gem_common_035.png</filename>
            <filename>tiles/gems/common/gem_common_036.png</filename>
            <filename>tiles/gems/common/gem_common_037.png</filename>
            <filename>tiles/gems/common/gem_common_038.png</filename>
            <filename>tiles/gems/common/gem_common_039.png</filename>
            <filename>tiles/gems/common/gem_common_040.png</filename>
            <filename>tiles/gems/common/gem_common_041.png</filename>
            <filename>tiles/gems/common/gem_common_042.png</filename>
            <filename>tiles/gems/common/gem_common_043.png</filename>
            <filename>tiles/gems/common/gem_common_044.png</filename>
            <filename>tiles/gems/common/gem_common_045.png</filename>
            <filename>tiles/gems/common/gem_common_046.png</filename>
            <filename>tiles/gems/common/gem_common_047.png</filename>
            <filename>tiles/gems/common/gem_common_048.png</filename>
            <filename>tiles/gems/common/gem_common_049.png</filename>
            <filename>tiles/gems/common/gem_common_050.png</filename>
            <filename>tiles/gems/common/gem_common_051.png</filename>
            <filename>tiles/gems/common/gem_common_052.png</filename>
            <filename>tiles/gems/common/gem_common_053.png</filename>
            <filename>tiles/gems/common/gem_common_054.png</filename>
            <filename>tiles/gems/common/gem_common_055.png</filename>
            <filename>tiles/gems/common/gem_common_056.png</filename>
            <filename>tiles/gems/common/gem_common_057.png</filename>
            <filename>tiles/gems/common/gem_common_058.png</filename>
            <filename>tiles/gems/common/gem_common_059.png</filename>
            <filename>tiles/gems/common/gem_common_060.png</filename>
            <filename>tiles/gems/common/gem_common_061.png</filename>
            <filename>tiles/gems/common/gem_common_062.png</filename>
            <filename>tiles/gems/common/gem_common_063.png</filename>
            <filename>tiles/gems/common/gem_common_064.png</filename>
            <filename>tiles/gems/common/gem_common_065.png</filename>
            <filename>tiles/gems/common/gem_common_066.png</filename>
            <filename>tiles/gems/common/gem_common_067.png</filename>
            <filename>tiles/gems/common/gem_common_068.png</filename>
            <filename>tiles/gems/common/gem_common_069.png</filename>
            <filename>tiles/gems/common/gem_common_070.png</filename>
            <filename>tiles/gems/common/gem_common_071.png</filename>
            <filename>tiles/gems/common/gem_common_072.png</filename>
            <filename>tiles/gems/common/gem_common_073.png</filename>
            <filename>tiles/gems/common/gem_common_074.png</filename>
            <filename>tiles/gems/common/gem_common_075.png</filename>
            <filename>tiles/gems/common/gem_common_076.png</filename>
            <filename>tiles/gems/common/gem_common_077.png</filename>
            <filename>tiles/gems/common/gem_common_078.png</filename>
            <filename>tiles/gems/common/gem_common_079.png</filename>
            <filename>tiles/gems/common/gem_common_080.png</filename>
            <filename>tiles/gems/common/gem_common_081.png</filename>
            <filename>tiles/gems/common/gem_common_082.png</filename>
            <filename>tiles/gems/common/gem_common_083.png</filename>
            <filename>tiles/gems/common/gem_common_084.png</filename>
            <filename>tiles/gems/common/gem_common_085.png</filename>
            <filename>tiles/gems/common/gem_common_086.png</filename>
            <filename>tiles/gems/common/gem_common_087.png</filename>
            <filename>tiles/gems/common/gem_common_088.png</filename>
            <filename>tiles/gems/common/gem_common_089.png</filename>
            <filename>tiles/gems/common/gem_common_090.png</filename>
            <filename>tiles/gems/common/gem_common_091.png</filename>
            <filename>tiles/gems/common/gem_common_092.png</filename>
            <filename>tiles/gems/common/gem_common_093.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
