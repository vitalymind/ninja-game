class Config {
    readonly WORLD_SIZE = 50
    readonly PATHFINDER_GRID_SIZE = this.WORLD_SIZE * 4
    readonly TILE_SIZE = 31
    readonly TILE_BLACK_FILL_CHANCE = 0.6
    readonly TILE_ROCK_FILL_VARIANTS = 12
    readonly TILE_ROCK_SIDES_VARIANTS = 4
    readonly TILE_SOIL_ROCKY_VARIANTS = 12
    readonly TILE_SOIL_GRASS_VARIANTS = 14
    readonly TILE_SOIL_PLAIN_VARIANTS = 4
    readonly TILE_GEM_COMMON_VARIANTS = 94
    readonly TILE_GEM_RARE_VARIANTS = 30
    readonly TILE_GEM_UNIQUE_VARIANTS = 10
    
    readonly CHARACTER_SIZE = 256
    readonly CHARACTER_SPEED_MOVE = 120 //Pixels per frame
    readonly CHARACTER_SPEED_ROTATE = 5 //Degree per frame
    readonly CHARACTER_PICKUP_RANGE = 80 //Pixels

    readonly OBJECTS_DYNOMITE_SIZE = 32
    readonly OBJECTS_DYNOMITE_EXPLODE_DELAY_S = 1
    readonly OBJECTS_DYNOMITE_FLY_SPEED = 250
    readonly OBJECTS_DYNOMITE_ROTATE_SPEED = 20

    readonly UI_BOTTOM_PADDING_PX = 10
    readonly UI_ELEMENT_SIZE_PX = 64
    readonly UI_LAYER_GROUND = 0
    readonly UI_LAYER_OBJECTS = 1
    readonly UI_LAYER_CHARACTER = 2
    readonly UI_LAYER_DEBUG = 100

    readonly RECIPE_DYNO_NITRATE = 1
    readonly RECIPE_DYNO_SULFUR = 4

    readonly VISIBILITY_RANGE = 8

    readonly LOOT_GEM_CHANCE = 0.25
    readonly LOOT_GEM_COMMON_CHANCE = 0.7
    readonly LOOT_GEM_IN_RESOURCES_CHANCE = 0.1

    readonly LOOT_GEM_AMOUNT_MIN = 1
    readonly LOOT_GEM_AMOUNT_MAX = 3
    
    readonly LOOT_RESOURCES_CHANCE = 0.3
    readonly LOOT_SULFUR_CHANCE = 0.5
    readonly LOOT_SULFUR_AMOUNT_MIN = 6
    readonly LOOT_SULFUR_AMOUNT_MAX = 15

    readonly LOOT_NITRATE_CHANCE = 0.5
    readonly LOOT_NITRATE_AMOUNT_MIN = 1
    readonly LOOT_NITRATE_AMOUNT_MAX = 4

    readonly AUDIO_EXPLOSION_VARIANTS = 5
    readonly AUDIO_CRYSTAL_PICKUP_VARIANTS = 3
}

const config = new Config()

export function formatedTime(givenSeconds: number): string {
    const hours = Math.floor(givenSeconds / 3600)
    const minutes = Math.floor((givenSeconds - (hours * 3600)) / 60)
    const secs = Math.floor((givenSeconds - (hours * 3600)) - (minutes * 60))

    return `${hours}:${minutes < 10? '0': ''}${minutes}:${secs < 10? '0': ''}${secs}`
}

export function lerp(p1: Point, p2: Point, fraction: number): Point {
    return new Point(p1.x * (1 - fraction) + p2.x * fraction, p1.y * (1 - fraction) + p2.y * fraction)
}

export function lerp1D(x:number, y:number, t: number):number {
    return x * (1 - t) + y * t
}

export function porabola(start:number, end: number, height: number, t: number): number {
    const f = (x:number): number => {return -4 * height * x * x + 4 * height * x}
    
    return (f(t) + lerp1D(start, end, t))
}

export function randomRange(min: number, max: number): number{
    return lerp1D(min, max, Math.random())
}

export function vectorNorm(vector: Point): Point {
    const len = Math.sqrt( vector.x * vector.x + vector.y * vector.y )
    if (len == 0) {
        return new Point(0,0)
    } else {
        return new Point(vector.x / len, vector.y / len)
    }
}

export function distance(from: Point, to: Point): number {
    return Math.sqrt(((from.x - to.x) * (from.x - to.x)) + ((from.y - to.y) * (from.y - to.y)))
}

export function degrees(radians: number): number {
    return radians * (180 / Math.PI)
}

export function radians(degree: number): number {
    return degree * (Math.PI / 180)
}

export function angleToVector(dir: number): [number, number] {
    return [Math.sin(radians(dir)), Math.cos(radians(dir))]
}

export function to2Digi(val: number): number {
    return (Math.floor(val * 100)) / 100
}

export function angleTo(fr: Point, to: Point) {
    let angle = degrees(Math.atan2(to.x-fr.x, to.y-fr.y))
    if (angle > 0) {
        return angle
    } else {
        angle += 360 
        if (angle == 360) return 0
        return angle
    }
}

export function drawDots(data: Array<Point>, time: number = 500): void {
    const cont = new PIXI.Container()
    cont.zIndex = config.UI_LAYER_DEBUG
    for (let i = 0; i < data.length; i++) {
        const g = new PIXI.Graphics().beginFill(0xffffff).drawCircle(data[i].x + config.TILE_SIZE/2, data[i].y + config.TILE_SIZE/2, 6).endFill()
        cont.addChild(g)
    }
    getLC().pixiScene.addChild(cont);
    setTimeout(()=>{getLC().pixiScene.removeChild(cont)},time)
}

export function getConfig(): Config {
    return config
}

export function Log(text: string): void {
    console.log(text)
}

export function getPrefixedRandomRange(min: number, max: number, salt: number): string{
    min = Math.floor(min)
    max = Math.floor(max)
    let res = Math.floor(salt * (max - min)) + min
    return res < 10 ? `0${res}` : `${res}`
}

let haltedOnError = false

export function haltOnCriticalError(errorMsg: string): void {
    toggleErrorScreen(true, errorMsg)
    haltedOnError = true
}

export function isHaltedOnError(): boolean {
    return haltedOnError
}

export function isWEBGLSupported(): boolean {
    const canvas = window.document.getElementById("three-canvas") as HTMLCanvasElement
    if (window.WebGLRenderingContext && ( canvas.getContext( 'webgl' ) || canvas.getContext( 'experimental-webgl' ))) return true
    return false
}

export function worldToCell(point: Point): Point {
    return new Point(Math.floor(point.x / config.TILE_SIZE), Math.floor(point.y / config.TILE_SIZE))
}

export function cellToWorld(point: Point): Point {
    return new Point(Math.floor(point.x * config.TILE_SIZE), Math.floor(point.y * config.TILE_SIZE))
}

export function worldToBlock(point: Point): Point {
    return new Point(Math.floor(point.x / config.TILE_SIZE / 4), Math.floor(point.y / config.TILE_SIZE / 4))
}

export function blockToWorld(point: Point): Point {
    return new Point(Math.floor(point.x * config.TILE_SIZE * 4), Math.floor(point.y * config.TILE_SIZE * 4))
}

export function isPosInBounds(point: Point): boolean {
    return (
        point.x < config.PATHFINDER_GRID_SIZE * config.TILE_SIZE &&
        point.x >= 0 &&
        point.y < config.PATHFINDER_GRID_SIZE * config.TILE_SIZE && 
        point.y >= 0
    )
}

export function posToBlockID(point: Point): number {
    return Math.floor(point.y / (config.TILE_SIZE * 4)) * config.WORLD_SIZE + Math.floor(point.x / (config.TILE_SIZE * 4))
}

export function posToCellID(point: Point): number {
    return Math.floor(point.y / config.TILE_SIZE) * config.PATHFINDER_GRID_SIZE + Math.floor(point.x / config.TILE_SIZE)
}

import { toggleErrorScreen } from "./ui"
import * as PIXI from 'pixi.js'
import { getLC } from "./loader"
import { Point } from "./pathfinding"
