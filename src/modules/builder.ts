import { getLC } from "./loader"
import * as PIXI from 'pixi.js'
import { getPrefixedRandomRange, getConfig, blockToWorld, randomRange, posToBlockID } from "./helper"
import { Point } from "./pathfinding"
import { Crystal, Nitrate, Sulfur } from "./gameManager"

const c = getConfig()

declare type Tile = "" | "fill" | "s" | "n" | "e" | "w" | "nw-o" | "ne-o"| "ne-c" | "nw-c" | "sw-o" | "se-o" | "se-c" | "sw-c"
export declare type Rarity = "unique" | "rare" | "common"
declare type Side = "bot" | "top" | "ul" | "ur" | "br" | "bl"
export declare type ResType = "sulfur" | "nitrate" | "crystal" | "none"
declare type TileType = "air" | "rock"
declare type Resource = {name: ResType, amount: number, rarity: Rarity, variant?: number}

export class Block {
    readonly pos: Point
    type: TileType = "air"
    walkable = Array<boolean>()
    rands = Array<number>()
    cont: PIXI.Container
    contain: Array<Resource> = []
    hasSulfur = false
    hasNitrate = false

    constructor(posX: number, posY: number) {
        this.pos = new Point(posX, posY)
        for (let i = 0; i < 16; i++){
            this.rands.push(Math.random())
            this.walkable.push(true)
        }
    }

    addResources(type: ResType, howMuch: number, howRare: Rarity = "common", variant?: number) {
        this.contain.push({name: type, amount: howMuch, rarity: howRare, variant})
    }

    explode(): void {
        const blockPos = blockToWorld(this.pos)
        const blockCenterWorldPos = new Point(blockPos.x + c.TILE_SIZE * 1.5, blockPos.y + c.TILE_SIZE * 1.5)
        for (const res of this.contain) {
            getLC().level.spawnResource(blockCenterWorldPos, res)
        }
    }
}

export class Level {
    blocks: Array<Block> = []
    
    constructor() {
        for (let y = 0; y < c.WORLD_SIZE; y++) {
            for (let x = 0; x < c.WORLD_SIZE; x++) {
                this.blocks.push(new Block(x ,y))
            }
        }
    }
    
    private getRandomSoil(salt: number): string {
        if (salt >= 0 && salt < 0.6) {
            return `soil_dirt_${getPrefixedRandomRange(0, c.TILE_SOIL_PLAIN_VARIANTS - 1, salt)}`
        } else if (salt >= 0.6 && salt < 0.8) {
            return `soil_grass_${getPrefixedRandomRange(0, c.TILE_SOIL_GRASS_VARIANTS - 1, salt)}`
        } else if (salt >= 0.8 && salt < 1) {
            return `soil_rock_${getPrefixedRandomRange(0, c.TILE_SOIL_ROCKY_VARIANTS - 1, salt)}`
        }
    }

    private getBlockByPos(posX: number, posY: number): Block {
        const id = posY * c.WORLD_SIZE + posX
        if (id < 0 || id >= this.blocks.length || posX < 0 || posX >= c.WORLD_SIZE || posY < 0 || posY >= c.WORLD_SIZE) return null
        return this.blocks[id]
    }

    private getNeighborsByCellPos(posX: number, posY: number) : Array<Block> {
        const thisCell = this.getBlockByPos(posX, posY)
        let result = []
        if (!thisCell) return null
        for (let pos of [[0,-1], [1,-1], [1,0], [1,1], [0,1], [-1,1], [-1,0], [-1,-1]]) {
            const block = this.getBlockByPos(posX + pos[0], posY + pos[1])
            result.push(block)
        }
        return result
    }

    private makeAir(thisBlock: Block, posX: number, posY: number): PIXI.Container {
        const lc = getLC()
        const c = getConfig()
        const cont = new PIXI.Container()
        cont.position.set(posX* c.TILE_SIZE * 4, posY * c.TILE_SIZE * 4)
        cont.zIndex = c.UI_LAYER_GROUND
        
        for (let y = 0; y < 4; y++) {
            for (let x = 0; x < 4; x++) {
                const salt = thisBlock.rands[y*4 + x]
                const name = this.getRandomSoil(salt)
                const sprite = new PIXI.Sprite(lc.spriteSheet.textures[`${name}.png`])
                sprite.position.set(x * c.TILE_SIZE, y * c.TILE_SIZE)
                cont.addChild(sprite)
            }
        }
        lc.pixiScene.addChild(cont)

    
        return cont
    }

    private adjustWalkable(block: Block): void {
        const blocks = getLC().level.getNeighborsByCellPos(block.pos.x, block.pos.y)
        const neighbours = [false,false,false,false,false,false,false,false]
        
        for (let i = 0; i < 16; i++) block.walkable[i] = (block.type == "air") ? true : (block.type == "rock") ? false : false

        if (block.type == "rock") {
            for (let x = 0; x < 8; x++) {
                if (!blocks[x] || blocks[x].type === "rock") neighbours[x] = true
            }
           
            if (!neighbours[0] && !neighbours[2]) block.walkable[3] = true
            if (!neighbours[2] && !neighbours[4]) block.walkable[15] = true
            if (!neighbours[4] && !neighbours[6]) block.walkable[12] = true
            if (!neighbours[6] && !neighbours[0]) block.walkable[0] = true
        }
        
    }

    private makeRock(thisBlock: Block, posX: number, posY: number): PIXI.Container {
        const lc = getLC()
        const c = getConfig()
        const cont = new PIXI.Container()
        cont.position.set(posX* c.TILE_SIZE * 4, posY * c.TILE_SIZE * 4)
        cont.zIndex = c.UI_LAYER_GROUND
        
        const blocks = lc.level.getNeighborsByCellPos(posX, posY)
        const neighbours = [false,false,false,false,false,false,false,false]
        for (let x = 0; x < 8; x++) {
            if (!blocks[x] || blocks[x].type === "rock") neighbours[x] = true
        }
    
        this.addMiddleSection("bot", thisBlock.rands, cont, neighbours[4], thisBlock.hasNitrate, thisBlock.hasSulfur)
        this.addMiddleSection("top", thisBlock.rands, cont, neighbours[0], thisBlock.hasNitrate, thisBlock.hasSulfur)
    
        this.addBorderSection("ur", thisBlock.rands, cont, [neighbours[0], neighbours[1], neighbours[2]], thisBlock.hasNitrate, thisBlock.hasSulfur)
        this.addBorderSection("ul", thisBlock.rands, cont, [neighbours[6], neighbours[7], neighbours[0]], thisBlock.hasNitrate, thisBlock.hasSulfur)
    
        this.addBorderSection("br", thisBlock.rands, cont, [neighbours[2], neighbours[3], neighbours[4]], thisBlock.hasNitrate, thisBlock.hasSulfur)
        this.addBorderSection("bl", thisBlock.rands, cont, [neighbours[4], neighbours[5], neighbours[6]], thisBlock.hasNitrate, thisBlock.hasSulfur)
        
        this.adjustWalkable(thisBlock)
        
        lc.pixiScene.addChild(cont)
    
        return cont
    }
    
    private addBorderSection(side: Side, salts: Array<number>, cont: PIXI.Container, neighbours: [boolean, boolean, boolean], nitrate: boolean, sulfir: boolean): void {
        let coords: Array<Array<number>> = 
            (side == "ur") ? [[3,0]] :
            (side == "ul") ? [[0,0]] :
            (side == "br") ? [[3,1],[3,2],[3,3]] :
            (side == "bl") ? [[0,1],[0,2],[0,3]] : [[]]
        let type: Tile
        let [n1, n2, n3] = neighbours
    
        if (n1 && n2 && n3) {                                               //X-X-X
            type = "fill"
    
        } else if ((!n1 && !n2 && !n3) || (!n1 && n2 && !n3)) {             //O-O-O | O-X-O
            type = 
                (side == "ur") ? "ne-o" : 
                (side == "ul") ? "nw-o" :
                (side == "br") ? "se-o" :
                (side == "bl") ? "sw-o" : ""
    
        } else if (!n1 && !n2 && n3) {                                      //O-O-X
            type = 
                (side == "ur") ? "n" :
                (side == "ul") ? "w" :
                (side == "br") ? "e" :
                (side == "bl") ? "s" : ""
    
        } else if ((n1 && !n2 && !n3) || (n1 && n2 && !n3)) {               //X-O-O | X-X-O
            type = 
                (side == "ur") ? "e" :
                (side == "ul") ? "n" :
                (side == "br") ? "s" :
                (side == "bl") ? "w" : ""
    
        } else if (!n1 && n2 && n3) {                                       //O-X-X
            type = 
                (side == "ur") ? "n" :
                (side == "ul") ? "w" :
                (side == "br") ? "e" :
                (side == "bl") ? "s" : ""
            
        } else if (n1 && !n2 && n3) {                                       //X-O-X
            type = 
                (side == "ur") ? "nw-c" :
                (side == "ul") ? "ne-c" :
                (side == "br") ? "sw-c" :
                (side == "bl") ? "se-c" : ""
        }
    
        let contents = nitrate ? 'nitrate' : sulfir ? 'sulfur' : 'plain'

        for (const cord of coords) {
            const salt = salts[ cord[0] * 4 + cord[1] ]
            const sprite = this.getRockSprite(type, salt, cord[1] - 1, "00", contents)
            sprite.position.set(cord[0] * c.TILE_SIZE, cord[1] * c.TILE_SIZE)
            cont.addChild(sprite)
        }
    }
    
    private addMiddleSection(side: Side, salts: Array<number>, cont: PIXI.Container, isClosed: boolean, nitrate: boolean, sulfir: boolean): void {
        let coords: Array<Array<number>>
        let type: Tile
        if (side === "top") {
            coords = [[1,0],[2,0]]
            type = isClosed ? "fill" : "n"
        } else if (side === "bot") {
            coords = [[1,1],[1,2],[1,3],[2,1],[2,2],[2,3]]
            type = isClosed ? "fill" : "s"
        }
        let contents = nitrate ? 'nitrate' : sulfir ? 'sulfur' : 'plain'
    
        let leftVariant = getPrefixedRandomRange(0,c.TILE_ROCK_SIDES_VARIANTS - 1, salts[0])
        let rightVariant = getPrefixedRandomRange(0,c.TILE_ROCK_SIDES_VARIANTS - 1, salts[1])
    
        for (const cord of coords) {
            const salt = salts[ cord[0] * 4 + cord[1] ]
            const sprite = this.getRockSprite(type, salt, cord[1] - 1, cord[0] == 1 ? leftVariant : rightVariant, contents)
            sprite.position.set(cord[0] * c.TILE_SIZE, cord[1] * c.TILE_SIZE)
            cont.addChild(sprite)
        }
    }
    
    private getRockSprite(type: Tile, salt: number, layer: number = 0, variant: string = "00", content: string): PIXI.Sprite | PIXI.Container {
        const lc = getLC()
        
        let contentName = content == "plain" ?  '' : `_${content}`

        let name = ''
        if (type === "fill") {
            name = (salt <= c.TILE_BLACK_FILL_CHANCE) ? 'rock_black_00' : `rock_fill_${getPrefixedRandomRange(0,c.TILE_ROCK_FILL_VARIANTS - 1, salt)}`
        } else {
            const cont = new PIXI.Container()
            if (type === "s") {
                name = `rock${contentName}_s_L${layer}_${variant}`
            } else if (type === "n") {
                name = `rock${contentName}_n_${variant}`
            } else if (type === "e") {
                name = `rock${contentName}_e_${getPrefixedRandomRange(0,c.TILE_ROCK_SIDES_VARIANTS - 1, salt)}`
            } else if (type === "w") {
                name = `rock${contentName}_w_${getPrefixedRandomRange(0,c.TILE_ROCK_SIDES_VARIANTS - 1, salt)}`
            
            } else if (type === "nw-o") {
                name = `rock${contentName}_open_nw_00`
            } else if (type === "ne-o") {
                name = `rock${contentName}_open_ne_00`
            } else if (type === "ne-c") {
                name = `rock${contentName}_close_ne_00`
            } else if (type === "nw-c") {
                name = `rock${contentName}_close_nw_00`

            } else if (type === "sw-o") {
                name = `rock${contentName}_open_sw_L${layer}_00`
            } else if (type === "se-o") {
                name = `rock${contentName}_open_se_L${layer}_00`
            } else if (type === "se-c") {
                name = `rock${contentName}_close_se_L${layer}_00`
            } else if (type === "sw-c") {
                name = `rock${contentName}_close_sw_L${layer}_00`
            }

            const soil = new PIXI.Sprite(lc.spriteSheet.textures[`${this.getRandomSoil(salt)}.png`])
            const rock = new PIXI.Sprite(lc.spriteSheet.textures[`${name}.png`])
            cont.addChild(soil)
            cont.addChild(rock)

            return cont
        } 
        return new PIXI.Sprite(lc.spriteSheet.textures[`${name}.png`])
    }
    
    private drawBlock(block: Block): void {
        const lc = getLC()
        let cont: PIXI.Container
        const id = (block.pos.y) * c.WORLD_SIZE +  (block.pos.x)
        if (block.type === "rock") {
            cont = this.makeRock(block, block.pos.x, block.pos.y)
        } else if (block.type == "air") {
            cont = this.makeAir(block, block.pos.x, block.pos.y)
        }
        block.cont = cont
        cont.visible = lc.visibleBlocks.includes(id) ? true : false
        lc.pixiScene.addChild(cont)
    }

    spawnResource(worldPos: Point, res: Resource): void {
        for (let i = 0; i < res.amount; i++) {
            const randomisedSpawnPos = new Point(worldPos.x + randomRange(-35,35), worldPos.y + randomRange(-35,35))
            if (res.name == "crystal") {
                new Crystal(randomisedSpawnPos, res.rarity, res.variant)
            } else if (res.name == "sulfur") {
                new Sulfur(randomisedSpawnPos)
            } else if (res.name == "nitrate") {
                new Nitrate(randomisedSpawnPos)
            }
        }
    }

    generateCave(): void {
        const lc = getLC()
        const centerBlockId = posToBlockID( new Point(c.TILE_SIZE * c.PATHFINDER_GRID_SIZE /2, c.TILE_SIZE * c.PATHFINDER_GRID_SIZE /2))
    
        //Fill world
        for (const block of lc.level.blocks) {
            if (Math.random() <= 0.95) {
                block.type = "rock"
                
                //Chance to contain some resources
                if (Math.random() < c.LOOT_RESOURCES_CHANCE) {
                    if (Math.random() > c.LOOT_NITRATE_CHANCE) {//Add sulfur
                        const howMuch = Math.floor(randomRange(c.LOOT_NITRATE_AMOUNT_MIN, c.LOOT_NITRATE_AMOUNT_MAX))
                        block.addResources("nitrate", howMuch)
                        block.hasNitrate = true
                    } else { //or nitrate
                        const howMuch = Math.floor(randomRange(c.LOOT_SULFUR_AMOUNT_MIN, c.LOOT_SULFUR_AMOUNT_MAX))
                        block.addResources("sulfur", howMuch)
                        block.hasSulfur = true
                    }

                    //Slim chance to have some common gems in resource blocks
                    if (Math.random() < c.LOOT_GEM_IN_RESOURCES_CHANCE) {
                        const howMuch = Math.floor(randomRange(c.LOOT_GEM_AMOUNT_MIN, c.LOOT_GEM_AMOUNT_MAX / 2))
                        block.addResources("crystal", howMuch, "common")
                    }

                } else {
                    //Add crystals
                    if (Math.random() < c.LOOT_GEM_CHANCE) {
                        if (Math.random() < c.LOOT_GEM_COMMON_CHANCE) { //Common
                            const howMuch = Math.floor(randomRange(c.LOOT_GEM_AMOUNT_MIN, c.LOOT_GEM_AMOUNT_MAX))
                            block.addResources("crystal", howMuch, "common")
                        } else { //rare
                            const howMuch = Math.floor(randomRange(c.LOOT_GEM_AMOUNT_MIN, c.LOOT_GEM_AMOUNT_MAX - 1))
                            block.addResources("crystal", howMuch, "rare")
                        }
                    }
                }

            } else {
                block.type = "air"
            }
        }

        //Distibute unique crystals
        for (let i = 0; i < c.TILE_GEM_UNIQUE_VARIANTS; i++) {
            let done = false
            while (!done) {
                const block = lc.level.blocks[Math.floor(randomRange(0, lc.level.blocks.length))]
                if (block.type == "rock" && block.contain.length == 0) {
                    block.addResources("crystal", 1, "unique", i)
                    done = true
                }
            }
        }

        //Make center block empty
        for (const i of [centerBlockId]) lc.level.blocks[i].type = "air"
        
        this.reDrawAll()
    }
    
    reDrawAll(): void {
        const lc = getLC()
        for (const block of this.blocks) {  
            lc.pixiScene.removeChild(block.cont)
        }

        for (let i = 0; i < this.blocks.length; i++) {
            this.drawBlock(this.blocks[i])
        }

        lc.pathfinder.updateWalkable(this.blocks)
    }

    reDrawPart(blockIDs: Array<number>): void {
        const lc = getLC()
        const affectedBlocks = Array<Block>()

        for (const id of blockIDs) {
            affectedBlocks.push(this.blocks[id])
            affectedBlocks.push(...this.getNeighborsByCellPos(this.blocks[id].pos.x, this.blocks[id].pos.y))
        }

        for (const block of affectedBlocks) {
            if (block) {
                this.adjustWalkable(block)
                lc.pixiScene.removeChild(block.cont)
                this.drawBlock(block)
            }
        }

        lc.pathfinder.updateWalkable(this.blocks)
    }
    
}

