import { moveCameraBy } from "./camera"
import { getLC } from "./loader"
import { handleClick } from "./gameManager"
import { getConfig } from "./helper"
import * as PIXI from 'pixi.js'

let dragging = false
let distanceDragged = 0
let dragStartX = 0
let dragStartY = 0

export function adjustUIPosition(): void {
    const c = getConfig()
    const lc = getLC()
    const scale = lc.pixiScene.scale.x
    const width = getWidth()
    const height = getHeight()
    
    //Arrange buttons
    const elementsCount = lc.pixiUI_buttons.children.length
    for (let i = 0; i < elementsCount; i++) {
        const elem = lc.pixiUI_buttons.children[i]
        elem.position.set(i * c.UI_ELEMENT_SIZE_PX, 0)
    }
    lc.pixiUI_buttons.position.set(
        (width * 0.3) - (elementsCount * c.UI_ELEMENT_SIZE_PX/2), height - c.UI_ELEMENT_SIZE_PX - c.UI_BOTTOM_PADDING_PX
    )

    //Arrange info
    let offset = 10
    const timeIndicator = lc.pixiUI_info.children[2]
    timeIndicator.position.set(width - timeIndicator.getBounds().width - 10, offset)
    offset += timeIndicator.getBounds().height

    const happinessIndicator = lc.pixiUI_info.children[3]
    happinessIndicator.position.set(width - happinessIndicator.getBounds().width - 10, offset)
    offset += happinessIndicator.getBounds().height + 10
    
    const sulfurIndicator = lc.pixiUI_info.children[0] as PIXI.Container
    let sprite = sulfurIndicator.children[0]
    sprite.scale.set((scale == 1) ? 0.125 : 0.25)
    let text = sulfurIndicator.children[1]
    sprite.position.set(text.getBounds().width + 10,-5)
    sulfurIndicator.position.set(width - sulfurIndicator.getBounds().width - 10, offset)
    offset += sulfurIndicator.getBounds().height
    
    const nitrateIndicator = lc.pixiUI_info.children[1] as PIXI.Container
    sprite = nitrateIndicator.children[0]
    sprite.scale.set((scale == 1) ? 0.125 : 0.25)
    text = nitrateIndicator.children[1]
    sprite.position.set(text.getBounds().width + 10,-2)
    nitrateIndicator.position.set(width - nitrateIndicator.getBounds().width - 10, offset)
}

export function updateUICounts(): void {
    const lc = getLC()
    const scale = lc.pixiScene.scale.x

    const sulfurIndicator = lc.pixiUI_info.children[0] as PIXI.Container
    let text = sulfurIndicator.children[1] as PIXI.Text
    text.style.fontSize = (scale == 1) ? 10 : 20
    text.text = `${lc.mainCharacter.sulfurCount} x`

    const nitrateIndicator = lc.pixiUI_info.children[1] as PIXI.Container
    text = nitrateIndicator.children[1] as PIXI.Text
    text.style.fontSize = (scale == 1) ? 10 : 20
    text.text = `${lc.mainCharacter.nitrateCount} x`

    const happinessIndicator = lc.pixiUI_info.children[3] as PIXI.Container
    text = happinessIndicator.children[0] as PIXI.Text
    text.style.fontSize = (scale == 1) ? 14 : 28
    text.text = `Happiness ${lc.mainCharacter.happiness}`
    
    const dynomiteButton = lc.pixiUI_buttons.children[0] as PIXI.Container
    text = dynomiteButton.children[2] as PIXI.Text
    text.text = `${lc.mainCharacter.dynomiteCount}`

    const timeIndicator = lc.pixiUI_info.children[2] as PIXI.Container
    text = timeIndicator.children[0] as PIXI.Text
    text.style.fontSize = (scale == 1) ? 14 : 28

    adjustUIPosition()
}

export function updateTimer(seconds: number): void {
    const lc = getLC()
    const scale = lc.pixiScene.scale.x

    const timeIndicator = lc.pixiUI_info.children[2] as PIXI.Container
    const text = timeIndicator.children[0] as PIXI.Text
    text.style.fontSize = (scale == 1) ? 14 : 28
    text.text = `Time ${seconds}`

    adjustUIPosition()
}

export function toggleLoadingScreen(toggle: boolean): void {
    let elem = window.document.getElementById("loading-screen")
    if (toggle) {
        elem.classList.remove("invisible")
        elem.classList.add("visible")
        elem.setAttribute('style', "display: flex;")
    } else {
        elem.classList.add("invisible")
        elem.classList.remove("visible")
        setTimeout(()=>{elem.setAttribute('style', "display: none;")}, 500)
    }
}

export function toggleErrorScreen(toggle: boolean, errorText: string): void {
    let screen = window.document.getElementById("error-screen")
    let text = window.document.getElementById("error-screen-text")
    if (toggle) {
        screen.setAttribute("style", "display: flex;")
        text.innerHTML = `Critical error, application halted!</br>Error: ${errorText}`
    } else {
        screen.setAttribute("style", "display: none;")
    }

}

export function setCorrectSceneScale(): void {
    const lc = getLC()
    let width = getWidth()
    let height = getHeight()
    if (width > 1000 && height > 700) {
        lc.pixiScene.scale.set(2)
    } else if ((width <= 1000) && (height <= 700) ) {
        lc.pixiScene.scale.set(1)
    }
}

export function handleInputEvent(type: string, event: PIXI.InteractionEvent): void {
    let inputX = event.data.global.x
    let inputY = event.data.global.y

    if(type === "down") {
        dragging = true
        distanceDragged = 0
        dragStartX = inputX
        dragStartY = inputY

    } else if(type === "drag") {
        if (dragging) {
            distanceDragged += Math.abs(dragStartX - inputX) + Math.abs(dragStartY - inputY)

            let offsetX = inputX - dragStartX
            let offsetY = inputY - dragStartY

            moveCameraBy(offsetX, offsetY)

            dragStartX = inputX
            dragStartY = inputY
        }
    } else if(type === "up") {
        if (dragging && distanceDragged > 10) {
        } else {
            handleClick(inputX, inputY)
        }
        dragging = false
    } else if(type === "out") {
        dragging = false
    }
}

export function handleKeyEvent(event: KeyboardEvent): void {
    const lc = getLC()
    if (event.code == "Space" ||  event.code == "Digit1") {
        if (lc.mainCharacter.dynomiteCount > 0) {
            toggleUIButtonByName("button_dynomite", lc.mainCharacter.toggleDynamite())
        }
    }
}

export function toggleUIButtonByName(name: string, state: boolean): void {
    toggleUIButton(getLC().menuButtons[name], state)
}

export function toggleUIButton(cont: PIXI.Container, state: boolean){
    const lc = getLC()
    const frame =  cont.getChildAt(0) as PIXI.Sprite
    frame.texture = lc.spriteSheet.textures[state ? 'button_on.png' : 'button_off.png']
    
    if (cont.name == "button_dynomite") {
        const item =  cont.getChildAt(1) as PIXI.Sprite
        item.texture = lc.spriteSheet.textures[state ? 'dynomite_on.png' : 'dynomite_off.png']
    }
}

export function getAspectRatio(): number {
    return window.innerWidth / window.innerHeight
}

export function getWidth(): number {
    return window.innerWidth
}

export function getHeight(): number {
    return window.innerHeight
}