import { Log, getConfig } from "./helper"
import * as THREE from 'three'
import * as PIXI from 'pixi.js'
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader"
import { getWidth, getHeight, handleInputEvent, setCorrectSceneScale, updateUICounts, handleKeyEvent } from "./ui"
import { centerCameraOnCharacter } from "./camera"
import { Level } from "./builder"
import { Pathfinder, Point } from "./pathfinding"
import { Character, Spawnable } from "./gameManager"


class LoaderContainer {
    //COUNTERS
    UITimerCounter = 0

    //MISC
    isLoadingComplete: boolean = false
    level: Level
    pathfinder: Pathfinder
    menuButtons: {[key:string]: PIXI.Container} = {}
    prevCharBlockPos: Point = new Point(0,0)
    
    //SFX
    sounds: {[key:string]: HTMLAudioElement} = {}

    //PIXI SCENE
    pixiApp: PIXI.Application
    pixiScene: PIXI.Container
    pixiUI: PIXI.Container
    pixiUI_buttons: PIXI.Container
    pixiUI_info: PIXI.Container
    UIEventManager: PIXI.InteractionManager
    visibleBlocks = Array<number>()

    //THREE SCENE
    threeRenderer: THREE.Renderer
    threeScene: THREE.Scene
    threeCamera: THREE.PerspectiveCamera

    //REUSABLE
    threeTextureLoader: THREE.TextureLoader
    GltfLoader: GLTFLoader

    //TEXTURES
    mainCharacterBaseColor: THREE.Texture
    mainCharacterMaterial: THREE.Material
    spriteSheet: PIXI.Spritesheet

    //MAIN CHARACTER
    mainCharacter: Character
    mainCharacterTexture: PIXI.Texture

    //SPAWNABLE OBJECTS
    spawnable = Array<Spawnable>()
    
}

const lc = new LoaderContainer()

//Private functions
function getCanvas(name: string): HTMLCanvasElement {
    if (name === "pixi") return  window.document.getElementById("pixi-canvas") as HTMLCanvasElement
    else if (name === "three") return  window.document.getElementById("three-canvas") as HTMLCanvasElement
}

async function loadTextures(): Promise<boolean> {
    return new Promise((resolve) => {
        PIXI.Loader.shared
        .add('./resources/tiles.json')
        .load(()=>{
            lc.spriteSheet = PIXI.Loader.shared.resources["./resources/tiles.json"].spritesheet
            
            resolve(true)
        })
    })
}

//Public functions
export function getLC(): LoaderContainer {
    return lc
}

export async function initLoader() : Promise<boolean> {
    return new Promise(async (resolve) => {
        Log("Loading started!")

        const c = getConfig()

        //Setup sfx
        for (const name of [
            "explosion_00","explosion_01","explosion_02","explosion_03","explosion_04",
            "pickup_00","pickup_crystal_00","pickup_crystal_01","pickup_crystal_02","throw_00"
        ]) {
            const sound = document.createElement("audio")
            sound.src = `./resources/${name}.mp3`
            sound.setAttribute("preload", "auto")
            sound.setAttribute("controls", "none")
            sound.style.display = "none"
            document.body.appendChild(sound)
            lc.sounds[name] = sound
        }

        //Setup pixi scene
        lc.pixiApp = new PIXI.Application({
            width: getWidth(), 
            height: getHeight(), 
            view: getCanvas('pixi'),
            backgroundAlpha: 0,
            antialias: true
        })
        lc.pixiScene = new PIXI.Container()
        lc.pixiScene.sortableChildren = true

        //Setup UI
        lc.pixiUI = new PIXI.Container()

        //Renderer eventhandler
        window.addEventListener('resize', () => {
            setCorrectSceneScale()
            lc.pixiApp.renderer.resize(getWidth(), getHeight())
            centerCameraOnCharacter()

            updateUICounts()
        }, true)

        //Load textures
        await loadTextures()

        //Setup three scene and camera
        lc.threeRenderer = new THREE.WebGLRenderer({canvas: getCanvas("three"), alpha: true})
        lc.threeRenderer.setSize(c.CHARACTER_SIZE, c.CHARACTER_SIZE)
        lc.threeScene = new THREE.Scene()
        lc.threeCamera = new THREE.PerspectiveCamera(10, 1, 0.1, 20)
        lc.threeCamera.position.set(4,12,0)
        lc.threeCamera.lookAt(-0.5,0,0)
        const spotLight = new THREE.SpotLight( 0xffffff )
        spotLight.position.set( 50, 100, 50 )
        lc.threeScene.add(spotLight)

        //Setup main character material
        lc.threeTextureLoader = new THREE.TextureLoader()
        lc.mainCharacterBaseColor = await lc.threeTextureLoader.loadAsync('./resources/ninja.png')
        lc.mainCharacterBaseColor.flipY = true
        lc.mainCharacterMaterial = new THREE.MeshPhongMaterial({map: lc.mainCharacterBaseColor})
        lc.mainCharacterMaterial.side = THREE.BackSide

        //Setup main character model
        lc.GltfLoader = new GLTFLoader()
        const obj = await lc.GltfLoader.loadAsync('./resources/cibus_ninja.glb')
        const skinnedMesh = obj.scene.children[0].children[1] as THREE.SkinnedMesh
        skinnedMesh.material = lc.mainCharacterMaterial
        lc.threeScene.add(obj.scene)

        //Setup pixi texture based on canvas rendered by three
        lc.mainCharacterTexture = PIXI.Texture.from(lc.threeRenderer.domElement)
        const sprite = PIXI.Sprite.from(lc.mainCharacterTexture)
        sprite.zIndex = c.UI_LAYER_CHARACTER
        lc.pixiScene.addChild(sprite)
        
        //Setup eventhandlers
        const manager = new PIXI.InteractionManager(lc.pixiApp.renderer);
        manager.on("pointerdown", (event: PIXI.InteractionEvent) => handleInputEvent('down', event))
        manager.on("pointermove", (event: PIXI.InteractionEvent) => handleInputEvent('drag', event))
        manager.on("pointerup", (event: PIXI.InteractionEvent) => handleInputEvent('up', event))
        manager.on("mouseout", (event: PIXI.InteractionEvent) => handleInputEvent('out', event))
        lc.UIEventManager = manager
        window.addEventListener('keydown', handleKeyEvent)

        //Setup map and pathfinder
        lc.level = new Level()
        lc.pathfinder = new Pathfinder()

        //Setup character
        lc.mainCharacter = new Character(sprite, obj.scene, new THREE.AnimationMixer(obj.scene), obj.animations)

        //Setup UI elements
        lc.pixiUI = new PIXI.Container()
        lc.pixiUI_buttons = new PIXI.Container()
        lc.pixiUI_info = new PIXI.Container()

        const dynomiteButton = new PIXI.Container()
        lc.menuButtons['button_dynomite'] = dynomiteButton
        dynomiteButton.addChild(new PIXI.Sprite(lc.spriteSheet.textures['dynomite_off.png']))
        dynomiteButton.addChild(new PIXI.Sprite(lc.spriteSheet.textures['button_off.png']))
        const dynomitecounter = new PIXI.Text('10',{fontFamily : 'Arial', fontSize: 18, fill : 0xffffff, align : 'right'})
        dynomitecounter.position.set(5, dynomiteButton.getBounds().height - 25)
        dynomiteButton.addChild(dynomitecounter)
        dynomiteButton.name = "button_dynomite"
        dynomiteButton.interactive = true;
        dynomiteButton.buttonMode = true;
        lc.pixiUI_buttons.addChild(dynomiteButton)

        const sulfurIndicator = new PIXI.Container()
        let spr = new PIXI.Sprite(lc.spriteSheet.textures['sulfur.png'])
        spr.scale.set(0.25)
        sulfurIndicator.addChild(spr)
        sulfurIndicator.addChild(new PIXI.Text('',{fontFamily : 'Arial', fontSize: 20, fill : 0xffffff, align : 'right'}))
        lc.pixiUI_info.addChild(sulfurIndicator)

        const nitrateIndicator = new PIXI.Container()
        spr = new PIXI.Sprite(lc.spriteSheet.textures['nitrate.png'])
        spr.scale.set(0.25)
        nitrateIndicator.addChild(spr)
        nitrateIndicator.addChild(new PIXI.Text('',{fontFamily : 'Arial', fontSize: 20, fill : 0xffffff, align : 'right'}))
        lc.pixiUI_info.addChild(nitrateIndicator)

        const timeIndicator = new PIXI.Container()
        timeIndicator.addChild(new PIXI.Text('Time 0',{fontFamily : 'Arial', fontSize: 28, fill : 0xffffff, align : 'right'}))
        lc.pixiUI_info.addChild(timeIndicator)

        const happinessIndicator = new PIXI.Container()
        happinessIndicator.addChild(new PIXI.Text('Happiness 0',{fontFamily : 'Arial', fontSize: 28, fill : 0xffffff, align : 'right'}))
        lc.pixiUI_info.addChild(happinessIndicator)

        lc.pixiUI.addChild(lc.pixiUI_buttons)
        lc.pixiUI.addChild(lc.pixiUI_info)

        lc.pixiApp.stage.addChild(lc.pixiScene)
        lc.pixiApp.stage.addChild(lc.pixiUI)
        
        setCorrectSceneScale()
        updateUICounts()
        lc.isLoadingComplete = true
        resolve(true)
    })
}