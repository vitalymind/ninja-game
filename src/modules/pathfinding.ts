import { getLC } from "./loader"
import { getConfig, drawDots } from "./helper"
import { Block } from "./builder"
import * as PIXI from 'pixi.js'

export class Point {
    x: number
    y: number 

    constructor(x: number, y:number) {
        this.set(x,y)
    }

    set(x: number, y:number): void {
        this.x = x
        this.y = y
    }
}
const c = getConfig()

//Single unit of pathfinding grid. Equals to one tile (32px x 32px)
class Slot {
    neighbors: Array<number>
    pos = new Point(0,0)
    searchIndex = 0
    parent = 0
    g = Infinity
    h = 0
    list = 0
    walkable = true
    constructor(x: number, y: number) {
        this.neighbors = []
        this.pos.set(x, y)
    }
}

export class Pathfinder {
    private slots: Array<Slot>
    private openList: Array<Map<string, number>>
    private currentSearch: number = 0

    constructor() {
        const lc = getLC()
        this.slots = []
        this.openList = []

        for (let y = 0; y < c.PATHFINDER_GRID_SIZE; y++) {
            for (let x = 0; x < c.PATHFINDER_GRID_SIZE; x++) {
                this.slots.push(new Slot(x,y))
            }
        }
    }
    
    private assignNeighbours(): void {
        for (let i = 0; i < this.slots.length; i++) {
            const slot = this.slots[i]
            const pos = this.idToPos(i)
            if (pos.x != 0 && this.isWalkable( pos.x - 1, pos.y )) slot.neighbors.push( this.posToId( pos.x - 1, pos.y ) )
            if (pos.y != 0 && this.isWalkable( pos.x, pos.y - 1 )) slot.neighbors.push( this.posToId( pos.x, pos.y - 1 ) )
            if (pos.x != c.PATHFINDER_GRID_SIZE - 1 && this.isWalkable( pos.x + 1, pos.y )) slot.neighbors.push( this.posToId( pos.x + 1, pos.y ) )
            if (pos.y != c.PATHFINDER_GRID_SIZE - 1 && this.isWalkable( pos.x, pos.y + 1 )) slot.neighbors.push( this.posToId( pos.x, pos.y + 1 ) )
        }
    }

    private dist(p1: Point, p2: Point): number {
        return Math.sqrt(((p1.x - p2.x) * (p1.x - p2.x)) + ((p1.y - p2.y) * (p1.y - p2.y)))
    }

    private distance(id_1: number, id_2: number): number {
        return this.dist(this.idToPos(id_1), this.idToPos(id_2))
    }
    
    private idToPos(id: number): Point {
        return new Point( Math.floor(id % c.PATHFINDER_GRID_SIZE), Math.floor(id / c.PATHFINDER_GRID_SIZE) )
    }

    private posToId(posX: number, posY: number): number {
        return posY * c.PATHFINDER_GRID_SIZE + posX
    }

    private isWalkable(posX: number, posY: number): boolean {
        return this.slots[this.posToId(posX, posY)].walkable
    }

    private lineOfSight(cell_id_1: number, cell_id_2: number): boolean {
        let l1 = this.idToPos(cell_id_1)
        let l2 = this.idToPos(cell_id_2)
        
        let diff = [l2.x - l1.x, l2.y - l1.y]
        let f = 0
        let dir = [0,0]
        let offset = [0,0]
        
        //X component
        if (diff[0] >= 0) {
            dir[0] = 1
            offset[0] = 1
        } else {
            diff[0] = -diff[0]
            dir[0] = -1
            offset[0] = -1
        }
        
        //Y component
        if (diff[1] >= 0) {
            dir[1] = 1
            offset[1] = 1
        } else {
            diff[1] = -diff[1]
            dir[1] = -1
            offset[1] = -1
        }
        if (diff[0] >= diff[1]) {
            while (l1.x != l2.x) {
                f += diff[1]
                if (f >= diff[0]) {
                    if (!this.isWalkable( l1.x + offset[0], l1.y + offset[1] )) return false
                    l1.y += dir[1]
                    f -= diff[0]
                }
                if (f != 0 && !this.isWalkable( l1.x + offset[0], l1.y + offset[1] )) return false
                
                if (diff[1] == 0 && !this.isWalkable( l1.x + offset[0], l1.y ) && !this.isWalkable( l1.x + offset[0], l1.y + 1 )) return false
                
                l1.x += dir[0]
            }
        } else {
            while (l1.y != l2.y) {
                f += diff[0]
                if (f >= diff[1]) {
                    if (!this.isWalkable( l1.x + offset[0], l1.y + offset[1] )) return false
                    
                    l1.x += dir[0]
                    f -= diff[1]
                }
                if (f != 0 && !this.isWalkable( l1.x + offset[0], l1.y + offset[1] )) return false
                
                if (diff[0] == 0 && !this.isWalkable( l1.x, l1.y + offset[1] ) && !this.isWalkable( l1.x + 1, l1.y + offset[1] )) return false
                
                l1.y += dir[1]
            }
        }
        return true
    }

    private findSortPos(value: number): number {
        for (let i = 0; i < this.openList.length; i++) {
            const item = this.openList[i].get("id")
            if (item == value) return i
        }
        return -1
    }

    private generateState(s: number, goal: number) {
        if (this.slots[s].searchIndex != this.currentSearch) {
            this.slots[s].searchIndex = this.currentSearch
            this.slots[s].h = this.distance(s, goal) * 100
            this.slots[s].g = Infinity
        }
    }

    private addToOpen(id: number): void {
        if (this.slots[id].list == 1) {
            const index = this.findSortPos(id)
            if (index != -1) this.openList.splice(id)
        } else {
            this.slots[id].list = 1
        }

        this.insertSorted( new Map<string, number>([['id', id], ['g', this.slots[id].g], ['f', this.slots[id].g + this.slots[id].h]]) )
    }

    private insertSorted(val: Map<string, number>): void {
        let index = 0
        for (let i = 0; i < this.openList.length; i++){
            const item = this.openList[i]
            if (Math.abs( item.get('f') - val.get('f') ) < 0.00001) {
                if (item.get('g') > val.get('g')) {
                    index = i
                    break
                }
            } else {
                if (item.get('f') < val.get('f')) {
                    index = i
                    break
                }
            }
        }

        this.openList.splice(index, 0, val)
    }

    private debugVisualizePath(path: Array<Point>): void {
        const tempList: Array<PIXI.Graphics> = []
        const lc = getLC()
        for (let i = 0; i < path.length; i++) {
            const tox = path[i].x
            const toy = path[i].y
            let frx, fry
            if (i == 0) {
                frx = lc.mainCharacter.position.x
                fry = lc.mainCharacter.position.y
            } else {
                frx = path[i-1].x * c.TILE_SIZE + (c.TILE_SIZE / 2)
                fry = path[i-1].y * c.TILE_SIZE + (c.TILE_SIZE / 2)
            }
            
            const graphics = new PIXI.Graphics();
            tempList.push(graphics)
            graphics.lineStyle(0.8, 0xffffff).moveTo(frx, fry).lineTo(tox * c.TILE_SIZE + (c.TILE_SIZE / 2), toy * c.TILE_SIZE + (c.TILE_SIZE / 2))
            graphics.zIndex = c.UI_LAYER_DEBUG
            lc.pixiScene.addChild(graphics);
        }
        setTimeout(()=>{
            for (const g of tempList) lc.pixiScene.removeChild(g)
        },1000)
    }

    updateWalkable(data: Array<Block>): void {
        const multiple = c.PATHFINDER_GRID_SIZE / c.WORLD_SIZE
        for (let i = 0; i < this.slots.length; i++) {
            const sPosX = this.slots[i].pos.x
            const sPosY = this.slots[i].pos.y
            const index = Math.floor(sPosY / multiple) * c.WORLD_SIZE + Math.floor( sPosX / multiple )
            const res = data[index].walkable[sPosY % 4 * multiple + sPosX % 4]
            this.slots[i].walkable = res
        }

        this.assignNeighbours()
    }

    findPath(fromPos: Point, toPos: Point): Array<Point> {
        const DEBUG = false //True to show path with drawn lines
        const DEBUG_DOTS = false //True to show visited slots

        if (
            fromPos.x >= c.PATHFINDER_GRID_SIZE || fromPos.x < 0 || 
            fromPos.y >= c.PATHFINDER_GRID_SIZE || fromPos.y < 0 || 
            toPos.x >= c.PATHFINDER_GRID_SIZE || toPos.x < 0 ||
            toPos.y >= c.PATHFINDER_GRID_SIZE || toPos.y < 0
        ) return []

        const fromID = this.posToId( fromPos.x, fromPos.y )
        const toID = this.posToId( toPos.x, toPos.y )
        const done = false
        let path = Array<Point>()

        if (this.lineOfSight(fromID, toID)) {
            path = [ new Point(toPos.x, toPos.y) ]

        } else {
            for (const c of this.slots) c.list = 0
            this.openList = []
            this.currentSearch += 1

            this.generateState(fromID, toID)
            this.generateState(toID, toID)
            
            this.slots[fromID].g = 0
            this.slots[fromID].parent = fromID
            
            this.addToOpen(fromID)
            
            while ((this.openList.length > 0) && this.slots[toID].g >= this.openList.at(-1).get('f') + 0.001) {
                const currId = this.openList.at(-1).get('id')
                this.slots[currId].list = 2;
                this.openList.pop()
                if (DEBUG_DOTS) drawDots([ new Point(this.slots[currId].pos.x * c.TILE_SIZE, this.slots[currId].pos.y * c.TILE_SIZE) ],3000);
                
                if (!this.lineOfSight(this.slots[currId].parent, currId)) {
                    this.slots[currId].g = Infinity
                    
                    for (const neighbor of this.slots[currId].neighbors) {
                        this.generateState(neighbor, toID)
                        if (this.slots[neighbor].list == 2) {
                            const newG = this.slots[neighbor].g + 1
                            if (newG < this.slots[currId].g) {
                                this.slots[currId].g = newG
                                this.slots[currId].parent = neighbor
                            }
                        }
                    }
                }

                for (const neighbor of this.slots[currId].neighbors) {
                    this.generateState(neighbor, toID)
                    const newParent = this.slots[currId].parent
                    if (this.slots[neighbor].list != 2) {
                        
                        const newG = this.slots[newParent].g + this.distance(newParent, neighbor)
                        
                        if (newG + 0.001 <= this.slots[neighbor].g) {
                            this.slots[neighbor].g = newG
                            this.slots[neighbor].parent = newParent
                            this.addToOpen(neighbor)
                        }
                    }
                }
            }
            
            if (this.slots[toID].g < Infinity) {
                let curr = toID
                while (curr != fromID) {
                    path.splice(0, 0, new Point( Math.floor(curr % c.PATHFINDER_GRID_SIZE), Math.floor(curr / c.PATHFINDER_GRID_SIZE) ) )
                    curr = this.slots[curr].parent
                }
            }
        }

        if (DEBUG) this.debugVisualizePath(path)
        
        const result = Array<Point>()
        for (const slot of path) result.push(new Point(slot.x * c.TILE_SIZE + (c.TILE_SIZE/2), slot.y * c.TILE_SIZE + (c.TILE_SIZE/2)))

        return result;
    }

    getlineOfSightPos(from: Point, to: Point): Point {
        const cell_id_1 = Math.floor(from.y / c.TILE_SIZE) * c.PATHFINDER_GRID_SIZE + Math.floor(from.x / c.TILE_SIZE)
        const cell_id_2 = Math.floor(to.y / c.TILE_SIZE) * c.PATHFINDER_GRID_SIZE + Math.floor(to.x / c.TILE_SIZE)

        let l1 = this.idToPos(cell_id_1)
        let l2 = this.idToPos(cell_id_2)
        
        let diff = [l2.x - l1.x, l2.y - l1.y]
        let f = 0
        let dir = [0,0]
        let offset = [0,0]

        
       //X component
       if (diff[0] >= 0) {
            dir[0] = 1
            offset[0] = 1
        } else {
            diff[0] = -diff[0]
            dir[0] = -1
            offset[0] = -1
        }
        
        //Y component
        if (diff[1] >= 0) {
            dir[1] = 1
            offset[1] = 1
        } else {
            diff[1] = -diff[1]
            dir[1] = -1
            offset[1] = -1
        }


        if (diff[0] >= diff[1]) {
            while (l1.x != l2.x) {
                f += diff[1]
                if (f >= diff[0]) {
                    if (!this.isWalkable( l1.x + offset[0], l1.y + offset[1] )) return new Point(l1.x*c.TILE_SIZE, l1.y*c.TILE_SIZE)
                    l1.y += dir[1]
                    f -= diff[0]
                }
                if (f != 0 && !this.isWalkable( l1.x + offset[0], l1.y + offset[1] )) return new Point(l1.x*c.TILE_SIZE, l1.y*c.TILE_SIZE)
                
                if (diff[1] == 0 && !this.isWalkable( l1.x + offset[0], l1.y ) && !this.isWalkable( l1.x + offset[0], l1.y + 1 )) return new Point(l1.x*c.TILE_SIZE, l1.y*c.TILE_SIZE)
                
                l1.x += dir[0]
            }
        } else {
            while (l1.y != l2.y) {
                f += diff[0]
                if (f >= diff[1]) {
                    if (!this.isWalkable( l1.x + offset[0], l1.y + offset[1] )) return new Point(l1.x*c.TILE_SIZE, l1.y*c.TILE_SIZE)
                    
                    l1.x += dir[0]
                    f -= diff[1]
                }
                if (f != 0 && !this.isWalkable( l1.x + offset[0], l1.y + offset[1] )) return new Point(l1.x*c.TILE_SIZE, l1.y*c.TILE_SIZE)
                
                if (diff[0] == 0 && !this.isWalkable( l1.x, l1.y + offset[1] ) && !this.isWalkable( l1.x + 1, l1.y + offset[1] )) return new Point(l1.x*c.TILE_SIZE, l1.y*c.TILE_SIZE)
                
                l1.y += dir[1]
            }
        }
        return new Point(l2.x*c.TILE_SIZE, l2.y*c.TILE_SIZE)
    }
}