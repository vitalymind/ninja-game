import { getLC } from "./loader"
import * as PIXI from 'pixi.js'
import { getConfig, angleTo, radians, distance, vectorNorm, lerp, porabola, worldToCell, isPosInBounds, cellToWorld, posToBlockID, worldToBlock, randomRange } from "./helper"
import * as THREE from 'three'
import { centerCameraOnCharacter } from "./camera"
import { Point } from "./pathfinding"
import { getHeight, getWidth, toggleUIButton, toggleUIButtonByName, updateTimer, updateUICounts } from "./ui"
import { handleGameWin } from "../index"
import { Rarity, ResType } from "./builder"

class Waypoint {
    startTimeS: number
    endTimeS: number
    startPos: Point
    endPos: Point

    constructor(startPos: Point, endPos: Point) {
        this.startPos = startPos
        this.endPos = endPos
    }
}

export class Spawnable {
    protected destroyed = false
    protected beingPickedUp = false
    protected pickingProgress = 0
    protected scale = 1
    
    protected sprite = new PIXI.Container()
    protected pickUpChar: Character
    pos: Point
    canBePickedUp: boolean
    type: ResType
    
    constructor(textureName:string, spawnPos: Point, scale: number, origSize: number, type:ResType, canPickup: boolean) {
        const lc = getLC()
        const c = getConfig()

        this.sprite.addChild( new PIXI.Sprite(lc.spriteSheet.textures[textureName]) )
        this.sprite.zIndex = c.UI_LAYER_OBJECTS
        this.sprite.position.set(spawnPos.x, spawnPos.y)
        this.sprite.scale.set(scale)
        this.sprite.pivot.set(origSize / 2, origSize / 2)
        this.pos = new Point(spawnPos.x, spawnPos.y)
        this.canBePickedUp = canPickup
        this.type = type
        this.scale = scale

        lc.pixiScene.addChild(this.sprite)
        lc.spawnable.push(this)
    }

    pickUp(character: Character): void {
        this.pickUpChar = character
        this.beingPickedUp = true
    }

    handlePickingUp(deltaTime: number): void {
        if (this.beingPickedUp) {
            this.pickingProgress += deltaTime * 5
            if (this.pickingProgress < 1) {
                const newPos = lerp(this.pos, this.pickUpChar.position, this.pickingProgress)
                this.sprite.scale.set(porabola(this.scale,0.1,0.3,this.pickingProgress))
                this.sprite.position.set(newPos.x, newPos.y)
            } else {
                this.destroy()
                this.pickUpChar.addItem(this.type)
                this.onPickedUp()
            }
        }
    }

    onPickedUp(): void{}

    isDestroyed(): boolean {
        return this.destroyed
    }

    destroy(): void {
        this.destroyed = true
        getLC().pixiScene.removeChild(this.sprite)
    }

    update(deltaTime: number): void {
    }
}

export class Dynomite extends Spawnable {
    private targetPos: Point
    private travelDistance: number
    private traveled = 0
    private lifeTimeS = 0

    constructor(throwFrom: Point, throwTo: Point) {
        super('dynomite_on.png', throwFrom, 0.5, 64, "none", false)
        const c = getConfig()

        //Calculate flight path
        const target = getLC().pathfinder.getlineOfSightPos(throwFrom, throwTo)
        this.targetPos = new Point(target.x+c.TILE_SIZE/2, target.y+c.TILE_SIZE/2)
        this.travelDistance = distance(new Point(throwFrom.x, throwFrom.y), this.targetPos)
        getLC().sounds[`throw_00`].play()
    }

    pickUp(): void {}

    destroy(): void {
        const lc = getLC()
        this.destroyed = true
        lc.pixiScene.removeChild(this.sprite)
    }

    update(deltaTime: number): void {
        const c = getConfig()
        const lc = getLC()

        this.lifeTimeS += deltaTime

        //Move toward target
        this.traveled += deltaTime * c.OBJECTS_DYNOMITE_FLY_SPEED
        const lerpedDist = this.traveled / this.travelDistance
        if (lerpedDist < 1) {
            this.sprite.rotation += deltaTime * c.OBJECTS_DYNOMITE_ROTATE_SPEED
            const newPos = lerp(this.pos, this.targetPos, lerpedDist)
            this.sprite.scale.set(porabola(0.5,0.5,0.3,lerpedDist))
            this.sprite.position.set(newPos.x, newPos.y)
        }

        //Check for explosion delay
        if (this.lifeTimeS >= c.OBJECTS_DYNOMITE_EXPLODE_DELAY_S) {
            const blocksInRange = Array<number>()
            const explosionCell = worldToCell(new Point(this.sprite.position.x, this.sprite.position.y))
            for (let x = -1; x < 2; x++) {
                for (let y = -1; y < 2; y++) {
                    const cell = new Point(explosionCell.x + x, explosionCell.y + y)
                    const cellWorldPos: Point = cellToWorld(cell)
                    if (isPosInBounds(cellWorldPos)) {
                        const blockID = posToBlockID(cellWorldPos)
                        if (!(blocksInRange.includes(blockID))) blocksInRange.push(blockID)
                    }
                }
            }
            for (const blockID of blocksInRange) {
                const block = lc.level.blocks[blockID]
                if (block.type == "rock") {
                    block.explode()
                    block.type = "air"
                }
            }

            fireExplosion(new Point(this.sprite.position.x, this.sprite.position.y))

            lc.level.reDrawPart(blocksInRange)

            this.destroy()
        }
    }
}

export class Crystal extends Spawnable {
    private rarity: Rarity

    constructor(spawnPos: Point, rarity: Rarity, forcedVariant: number) {
        const c = getConfig()
        let name: string
        let variant: number
        let scale = 0.5
        if (rarity == "common") {
            variant = Math.floor(randomRange(0, c.TILE_GEM_COMMON_VARIANTS ))
            name = `gem_common_${variant < 10 ? '00' : variant < 100 ? '0' : ''}${variant}`
            scale = 0.35
        } else if (rarity == "rare") {
            variant = Math.floor(randomRange(0, c.TILE_GEM_RARE_VARIANTS ))
            name = `gem_rare_${variant < 10 ? '00' : variant < 100 ? '0' : ''}${variant}`
            scale = 0.55
        } else if (rarity == "unique") {
            name = `gem_unique_${forcedVariant < 10 ? '00' : forcedVariant < 100 ? '0' : ''}${forcedVariant}`
            scale = 0.8
        }

        super(`${name}.png`, spawnPos, scale, 64, "crystal", true)
        this.rarity = rarity
    }

    update(deltaTime: number): void {
        this.handlePickingUp(deltaTime)
    }

    onPickedUp(): void {
        const lc = getLC()
        lc.sounds[`pickup_crystal_0${Math.floor(randomRange(0,getConfig().AUDIO_CRYSTAL_PICKUP_VARIANTS))}`].play()

        let happiness: number
        let scale: number
        let color: number
        let text: string
        let weight: PIXI.TextStyleFontWeight
        if (this.rarity == "common") {
            scale = 0.5
            color = 0x7CFC00
            happiness = Math.floor(randomRange(10,15))
            weight = "100"
            text = `Common +${happiness}`
            lc.mainCharacter.foundCommon += 1
        } else if (this.rarity == "rare") {
            scale = 0.7
            happiness = Math.floor(randomRange(50,70))
            color = 0x8F00FF
            weight = "400"
            text = `Rare +${happiness}`
            lc.mainCharacter.foundRare += 1
        } else if (this.rarity == "unique") {
            scale = 1
            happiness = Math.floor(1500)
            color = 0xFFAC1C
            weight = "600"
            text = `Unique +${happiness}`
            lc.mainCharacter.foundUnique += 1
        }


        const pixiText = new PIXI.Text(text,{fontFamily : 'Arial', fontSize: 100 * scale, fill : color, align : 'center', fontWeight: weight})
        lc.mainCharacter.sprite.addChild(pixiText)

        const interval = setInterval(()=>{
            if (pixiText) {
                pixiText.position.y -= 2
            }
        }, 10)
1
        setTimeout(()=>{
            lc.mainCharacter.sprite.removeChild(pixiText)
            clearInterval(interval)
        },1500)

        this.pickUpChar.addHappiness(happiness)
    }
}

export class Sulfur extends Spawnable {
    constructor(spawnPos: Point) {
        super("sulfur.png", spawnPos, 0.25, 64, "sulfur", true)
    }
    update(deltaTime: number): void {
        this.handlePickingUp(deltaTime)
    }
    onPickedUp(): void{
        getLC().sounds["pickup_00"].play()
    }
}

export class Nitrate extends Spawnable {
    constructor(spawnPos: Point) {
        super("nitrate.png", spawnPos, 0.25, 64, "nitrate", true)
    }
    update(deltaTime: number): void {
        this.handlePickingUp(deltaTime)
    }
    onPickedUp(): void{
        getLC().sounds["pickup_00"].play()
    }
}

export class Character {
    private waypoints: Array<Waypoint> = []
    sprite: PIXI.Container
    private threeObj: THREE.Group
    animMixer: THREE.AnimationMixer
    private animations: THREE.AnimationClip[]
    private dynomiteActive: boolean = false

    happiness: number = 0 // hapiness is a number obviously =)
    direction: number = 0
    position = new Point(0, 0)
    moveSpeed: number
    rotateSpeed: number

    dynomiteCount = 10
    sulfurCount = 0
    nitrateCount = 0

    foundCommon = 0
    foundRare = 0
    foundUnique = 0

    constructor(sprite: PIXI.Container, threeObj: THREE.Group, animMixer: THREE.AnimationMixer, animations: THREE.AnimationClip[]) {
        const c = getConfig()

        this.sprite = sprite
        this.moveSpeed = c.CHARACTER_SPEED_MOVE
        this.rotateSpeed = c.CHARACTER_SPEED_ROTATE
        sprite.pivot.set(c.CHARACTER_SIZE/2,c.CHARACTER_SIZE/2)
        sprite.scale.set(0.25)
        sprite.zIndex = c.UI_LAYER_CHARACTER

        this.threeObj = threeObj
        this.animMixer = animMixer
        this.animations = animations
        this.threeObj.position.set(0,0,0)
        this.animMixer.clipAction(THREE.AnimationClip.findByName(this.animations, 'Armature|idle')).play()
    }

    addHappiness(howMuch: number): void {
        this.happiness += howMuch
        updateUICounts()
    }

    addItem(item: ResType): void {
        if (item == "sulfur") this.sulfurCount += 1
        else if (item == "nitrate") this.nitrateCount += 1
        updateUICounts()
    }

    toggleDynamite(): boolean {
        if (this.dynomiteActive) {
            this.dynomiteActive = false
        } else {
            this.dynomiteActive = true
        }
        return this.dynomiteActive
    }

    isDynomiteActive(): boolean {
        return this.dynomiteActive
    }

    setWaypoints(points: Array<Point>): void {
        this.waypoints = []

        for (let i = 0; i < points.length; i++) {
            let to: Point = points[i]
            let from : Point
            if (i == 0) {
                from = this.position
            } else {
                from = points[i-1]
            }
            if (i < points.length) {
                this.waypoints.push(
                    new Waypoint(from, to)
                )
            }
        }
    }

    moveToCoords(posX: number, posY: number): void {
        this.sprite.position.set(posX, posY)
        this.position.set(posX, posY)
    }

    moveToPoint(point: Point): void {
        this.sprite.position.set(point.x, point.y)
        this.position = point
    }

    update(deltaTime: number): void {
        const lc = getLC()
        const c = getConfig()

        //Moving character
        if(this.waypoints.length > 0) {
            //Rotate towards waypoint
            const curwp = this.waypoints[0]
            const targetRot = radians(angleTo(this.position, curwp.endPos) + 100)
            const len = this.threeObj.rotation.y - targetRot
            if (Math.abs(len) > 0.1) {
                this.threeObj.rotation.y += (len > 0) ? -0.1 : 0.1 
            }

            //Move forward
            const vectordir = vectorNorm( new Point(this.position.x - curwp.endPos.x, this.position.y - curwp.endPos.y) )
            const velocity = new Point(deltaTime * vectordir.x * this.moveSpeed, deltaTime * vectordir.y * this.moveSpeed)
            this.position.x -= velocity.x
            this.position.y -= velocity.y
            this.sprite.position.x -= velocity.x
            this.sprite.position.y -= velocity.y

            //Check for destination
            if (distance( this.position, curwp.endPos ) < 1) {
                this.waypoints.splice(0,1)
            }
        }

        //Check for nearby pickup
        for (const item of lc.spawnable) {
            if (item.canBePickedUp) {
                const dist = distance(this.position, item.pos)
                if (dist < c.CHARACTER_PICKUP_RANGE) {
                    item.pickUp(this)
                }
            }
        }

        //Auto-crafting
        if (this.sulfurCount >= c.RECIPE_DYNO_SULFUR && this.nitrateCount >= c.RECIPE_DYNO_NITRATE) {
            this.dynomiteCount += 1
            this.sulfurCount -= c.RECIPE_DYNO_SULFUR
            this.nitrateCount -= c.RECIPE_DYNO_NITRATE
            updateUICounts()
        }
    }
}

function fireExplosion(position: Point) {
    //TODO: Move to own class and handle in update loop
    const lc = getLC()
    const c = getConfig()
    const explosion = new PIXI.AnimatedSprite(lc.spriteSheet.animations["explosion"])
    explosion.position.set(position.x, position.y)
    explosion.zIndex = 5
    explosion.animationSpeed = 1.5;
    explosion.pivot.set(32,32)
    explosion.loop = false
    explosion.play()
    lc.pixiScene.addChild(explosion);
    getLC().sounds[`explosion_0${Math.floor(randomRange(0,c.AUDIO_EXPLOSION_VARIANTS))}`].play()

    const interval = setInterval(()=>{
        explosion.scale.x += 0.04
        explosion.scale.y += 0.04
    }, 1);
    
    const timeout = setTimeout(()=>{
        lc.pixiScene.removeChild(explosion)
        clearInterval(interval)
        clearTimeout(timeout)
    },1500)

    lc.mainCharacter.addHappiness(Math.floor(randomRange(3,8)))
}

export async function resetWorld(): Promise<boolean> {
    return new Promise(async (resolve) => {
        const lc = getLC()
        const c = getConfig()
        
        //Destroy all spawned objects
        for (let i = 0; i < lc.spawnable.length; i++) lc.spawnable[i].destroy()
        lc.spawnable = []
        
        
        lc.level.generateCave()

        //Reset pixi scene
        const worldCenter = new Point(c.TILE_SIZE * c.PATHFINDER_GRID_SIZE /2 + c.TILE_SIZE*2, c.TILE_SIZE * c.PATHFINDER_GRID_SIZE /2 + c.TILE_SIZE*2)
        lc.mainCharacter.moveToPoint(worldCenter)

        //Reset camera
        centerCameraOnCharacter()

        resolve(true)
    })
}

export function render(deltaTime: number): void {
    const lc = getLC()

    //Render main character with three
    lc.mainCharacter.animMixer.update( deltaTime )
    lc.threeRenderer.render(lc.threeScene, lc.threeCamera)
    
    //Render rest with pixi
    lc.mainCharacterTexture.update()
    lc.pixiApp.renderer.render(lc.pixiApp.stage)
}

export function update(deltaTime: number, appTime: number): void {
    const lc = getLC()
    
    //Update UI time
    lc.UITimerCounter += deltaTime
    if (lc.UITimerCounter >= 1) {
        lc.UITimerCounter = 0
        updateTimer(Math.floor(appTime))

        //Check camera too far
        const screenPos = new Point((getWidth()/2 - lc.pixiScene.position.x) / lc.pixiScene.scale.x, (getHeight()/2 - lc.pixiScene.position.y) / lc.pixiScene.scale.y)
        if (distance(screenPos, lc.mainCharacter.position) > 600) {
            centerCameraOnCharacter()
        }
    }
    
    //Update visible blocks
    const newBlockPos = worldToBlock(lc.mainCharacter.position)
    if (newBlockPos.x != lc.prevCharBlockPos.x || newBlockPos.y != lc.prevCharBlockPos.y) {
        lc.prevCharBlockPos = newBlockPos
        updateVisibility()
    }

    //Update character
    lc.mainCharacter.update(deltaTime)

    //Update spawned objects
    const toBeRemoved = Array<number>()
    for (let i = 0; i < lc.spawnable.length; i++) {
        const item = lc.spawnable[i]
        if (item.isDestroyed()) {
            toBeRemoved.push(i)
        } else {
            item.update(deltaTime)
        }
    }
    if (toBeRemoved.length > 0) {
        toBeRemoved.sort((a:number,b:number)=>b-a)
        for(const i of toBeRemoved) lc.spawnable.splice(i,1)
    }

    //Check for game over
    if (lc.mainCharacter.dynomiteCount == 0) {
        if (!lc.spawnable.some((item: Spawnable) => distance(lc.mainCharacter.position, item.pos) < 1000)) {
            handleGameWin()
        }
    }
}

function updateVisibility(): void {
    const lc = getLC()
    const c = getConfig()

    //Get all nearby blocks
    const nearBlocks = Array<number>()
    for (let y = c.VISIBILITY_RANGE * -1; y <= c.VISIBILITY_RANGE; y++) {
        for (let x = c.VISIBILITY_RANGE * -1; x <= c.VISIBILITY_RANGE; x++) {
            const blocPos = worldToBlock(lc.mainCharacter.position)
            const id = (blocPos.y + y) * c.WORLD_SIZE +  (blocPos.x + x)
            if (id >= 0 && id < lc.level.blocks.length ) nearBlocks.push(id)
        }
    }

    //Get not yet rendered blocks
    const newBlocks = Array<number>()
    for (const i of nearBlocks) {
        if (!lc.visibleBlocks.includes(i)) {
            newBlocks.push(i)
            lc.visibleBlocks.push(i)
        }
    }

    //Render new blocks
    for (const i of newBlocks) lc.level.blocks[i].cont.visible = true


    //Get blocks that are too far away
    const farawayBlocks = Array<number>()
    for (const i of lc.visibleBlocks) {
        if (!nearBlocks.includes(i)) {
            farawayBlocks.push(i)
            lc.level.blocks[i].cont.visible = false
        }
    }

    //Clear out faraway blocks
    for (const i of farawayBlocks) {
        lc.visibleBlocks.splice(lc.visibleBlocks.indexOf(i), 1)
    }
}

export async function handleClick(screenX: number, screenY: number) {   
    const lc = getLC()
    const c = getConfig()
    const target = lc.UIEventManager.hitTest(new PIXI.Point(screenX, screenY))
    const worldPosX = (screenX - lc.pixiScene.position.x) / lc.pixiScene.scale.x
    const worldPosY = (screenY - lc.pixiScene.position.y) / lc.pixiScene.scale.x
    const charPosX = lc.mainCharacter.position.x
    const charPosY = lc.mainCharacter.position.y
    
    //Handle UI click
    if (target && target.name) {
        if (target.name === "button_dynomite") {
            if (lc.mainCharacter.dynomiteCount > 0) {
                toggleUIButton(target as PIXI.Sprite, lc.mainCharacter.toggleDynamite())
            }
        }
        return
    }
    
    //Clicked out of bounds 
    if (!isPosInBounds(new Point(worldPosX, worldPosY))) {
        return
    }
    
    /*
    const clickedCellID = posToCellID(new Point(worldPosX, worldPosY))
    const clickedBlockID = posToBlockID(new Point(worldPosX, worldPosY))
    const charPosCellID = posToCellID(new Point(charPosX, charPosY))
    console.log(`Click in world at [${worldPosX}|${worldPosY}] block id: ${clickedBlockID} cell id: ${clickedCellID}\nChar pos [${charPosX}|${charPosY}] cell id:${charPosCellID}`)
    */

    if (lc.mainCharacter.isDynomiteActive()) {
        //Handle dynomite throwing
        if (lc.mainCharacter.dynomiteCount > 0) {
            lc.mainCharacter.dynomiteCount -= 1
            const dynomite = new Dynomite(new Point(charPosX, charPosY), new Point(worldPosX, worldPosY))
            toggleUIButtonByName("button_dynomite", lc.mainCharacter.toggleDynamite())
            updateUICounts()
        }

    } else {
        //Handle movement
        let path = lc.pathfinder.findPath(
            new Point(Math.floor(charPosX / c.TILE_SIZE), Math.floor(charPosY / c.TILE_SIZE)), 
            new Point(Math.floor(worldPosX / c.TILE_SIZE), Math.floor(worldPosY / c.TILE_SIZE))
        )
        lc.mainCharacter.setWaypoints(path)
    }
}
