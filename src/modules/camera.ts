import { getWidth, getHeight } from "./ui"
import { getLC } from "./loader"

export function centerCameraOnCharacter(): void {
    const lc = getLC()

    moveCameraTo(
        (getWidth()/2) - (lc.mainCharacter.position.x * lc.pixiScene.scale.x),
        (getHeight()/2) - (lc.mainCharacter.position.y * lc.pixiScene.scale.x),
    )

}

export function moveCameraTo(x: number, y: number): void {
    const lc = getLC()
    lc.pixiScene.position.set( x, y )
}

export function moveCameraBy(x: number, y: number): void {
    const lc = getLC()
    lc.pixiScene.position.x += x
    lc.pixiScene.position.y += y
}