import * as THREE from 'three'
import * as PIXI from 'pixi.js'
import { getLC, initLoader } from "./modules/loader"
import { toggleLoadingScreen } from "./modules/ui"
import { isHaltedOnError, haltOnCriticalError, isWEBGLSupported } from "./modules/helper"
import { resetWorld, render, update } from "./modules/gameManager"

const clock = new THREE.Clock()
let appTime = 0
let fpsCounter = 0
let deltaTime = 0
let pause = false

declare type MainLoopState = "preload" | "loading" | "inprogress" | "win"
let state: MainLoopState = "preload"
const fpsMeter = window.document.getElementById("fps-counter")

document.addEventListener("visibilitychange", function() {
    if (document.visibilityState === 'visible') {
        pause = false
        clock.getDelta()
    } else {
        pause = true
    }1
})

async function loop() : Promise<void> {
    if (!pause) {
        deltaTime = clock.getDelta()
        appTime += deltaTime
        
        //Debug staff
        fpsCounter += deltaTime
        if (fpsCounter > 0.1) {
            fpsCounter = 0
            fpsMeter.innerHTML = (Math.floor(1 / deltaTime)).toString()
        }
    
        if (state === "preload") {
            toggleLoadingScreen(true)
    
            if (!PIXI.utils.isWebGLSupported || !isWEBGLSupported()) haltOnCriticalError("Browser does not support WEBGL")
    
            if (!await initLoader()) haltOnCriticalError("Failed to load assets")
    
            state = "loading"
        
        } else if (state === "loading") {
            await resetWorld()
            toggleLoadingScreen(false)
            
            state = "inprogress"
        } else if (state === "inprogress") {
            update(deltaTime, appTime)
            render(deltaTime)
    
        } else if (state === "win") {
            document.getElementById("end-screen").setAttribute("style", "display: flex;")
        }
    
        if (isHaltedOnError()) return
        requestAnimationFrame(loop)
    }
}

export function getAppTime(): number {
    return appTime
}

export function handleGameWin(): void {
    const lc = getLC()
    document.getElementById("time").innerHTML = `${Math.floor(appTime * 100) / 100}`
    document.getElementById("happiness").innerHTML = `${lc.mainCharacter.happiness}`
    document.getElementById("common").innerHTML = `${lc.mainCharacter.foundCommon}`
    document.getElementById("rare").innerHTML = `${lc.mainCharacter.foundRare}`
    document.getElementById("unique").innerHTML = `${lc.mainCharacter.foundUnique}`
    state = "win"

    //Score submiting and display
    const button = document.getElementById("submit_button") as HTMLButtonElement
    button.addEventListener('click', async ()=>{
        const name = document.getElementById("name") as HTMLInputElement
        if (name.value === "") return

        document.getElementById("submit").setAttribute("style", "display: none")
        
        const postData = JSON.stringify({
            name: name.value,
            score: lc.mainCharacter.happiness,
            time: Math.floor(getAppTime() * 100) / 100,
            common: lc.mainCharacter.foundCommon,
            rare: lc.mainCharacter.foundRare,
            unique: lc.mainCharacter.foundUnique,
            uploadTime: Date.now()
        })
        const response = await fetch('/ninja/submit', {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: postData
        })
        
        const leaders = await response.json()
        if (leaders) {
            let text = ""
            for (const record of leaders.data) {
                text += `
                <tr>
                    <td>${record.place}</td><td>${record.name}</td><td>${record.score}</td><td>${record.time}</td>
                    <td><span class="common">${record.common}</span> | <span class="rare">${record.rare}</span> | <span class="unique">${record.unique}</span></td>
                </tr>
                `
            }
            if (leaders.ownScore.place > 10) {
                text += `
                <tr><td>...</td><td></td><td></td><td></td><td></td></tr>
                <tr>
                    <td>${leaders.ownScore.place}</td>
                    <td>${leaders.ownScore.name}</td>
                    <td>${leaders.ownScore.score}</td>
                    <td>${leaders.ownScore.time}</td>
                    <td><span class="common">${leaders.ownScore.common}</span> | <span class="rare">${leaders.ownScore.rare}</span> | <span class="unique">${leaders.ownScore.unique}</span></td>
                </tr>
                `
            }

            document.getElementById("results").setAttribute("style", "display: flex")
            const resultsElem = document.getElementById("results-table")
            resultsElem.innerHTML = `
            
            <div>
                <table>
                    <tr>
                        <th>Place</th>
                        <th>Name</th>
                        <th>Score</th>
                        <th>Time</th>
                        <th><span class="common">C</span> | <span class="rare">R</span> | <span class="unique">U</span></th>
                    </tr>
                    ${text}
                </table>
            </div>
            
            `
        }
    })
}

window.requestAnimationFrame(loop)