const express = require('express')
const path = require('path')
const fs = require('fs')
const app = express()
const port = 3080

app.use(express.json());

app.post('/ninja/submit/', (req, res) => {
    //Check input
    if (
        req.body.name === undefined || typeof req.body.name !== 'string' ||
        req.body.score === undefined || typeof req.body.score !== 'number' ||
        req.body.time === undefined || typeof req.body.time !== 'number' ||
        req.body.common === undefined || typeof req.body.common !== 'number' ||
        req.body.rare === undefined || typeof req.body.rare !== 'number' ||
        req.body.unique === undefined || typeof req.body.unique !== 'number' ||
        req.body.uploadTime === undefined || typeof req.body.uploadTime !== 'number'
    ) {
        res.sendStatus(404)
        return
    }
        
    //Read leaderboard from file
    const rawdata = fs.readFileSync(path.resolve(__dirname, "./leaderboard.json"))
    let leaderboard = JSON.parse(rawdata)
	
    //Put new entry in
	let yourScore = {
		place: -1,
		name: req.body.name,
		score: req.body.score,
		time: req.body.time,
		common: req.body.common,
		rare: req.body.rare,
		unique: req.body.unique,
		uploadTime: req.body.uploadTime
	}
	leaderboard.data.push(yourScore)

	//Sort by score
	leaderboard.data = leaderboard.data.sort((a,b)=>{
		if ( a.score > b.score ) return -1
		if ( a.score < b.score ) return 1
		return 0
	})

	//Prepare for sending back scores
	let toBeSent = []
	for (let i = 0; i < leaderboard.data.length; i++) {
		const dt = leaderboard.data[i]
		dt.place = i + 1
		if (i < 10) {
			toBeSent.push(dt)
		}
	}

	//Save file
	fs.writeFileSync(path.resolve(__dirname, "./leaderboard.json"), JSON.stringify(leaderboard));

	//Compile list of top player and send back
	res.setHeader('Content-Type','application/json')
	res.send(JSON.stringify({ data: toBeSent, ownScore: yourScore}))
});

app.get('/ninja/leaderboard/', (req, res) => {
    const rawdata = fs.readFileSync(path.resolve(__dirname, "./leaderboard.json"))
    let leaderboard = JSON.parse(rawdata)
    let html = `
    <html><head></head><body>
    <div><table>
        <tr>
            <th>Place</th>
            <th>Name</th>
            <th>Score</th>
            <th>Time</th>
            <th>Common</th>
            <th>Rare</th>
            <th>Unique</th>
            <th>Uploaded</th>
        </tr>`
    
    for (const item of leaderboard.data) {
        html += `
        <tr>
            <td>${item.place}</td>
            <td>${item.name}</td>
            <td>${item.score}</td>
            <td>${item.time}</td>
            <td>${item.common}</td>
            <td>${item.rare}</td>
            <td>${item.unique}</td>
            <td>${new Date(item.uploadTime).toGMTString()}</td>
        </tr>
        `
    }

    html += `</table></div></body><style>
		table, th, tr, td{
			padding-left: 20px;
			padding-right: 20px;
			text-aling: left;
		}
	</style></html>`

    res.setHeader('Content-Type','text/html')
    res.send(html)
})

app.listen(port);